<%@ page language="C#" masterpagefile="~/AppQuote.master" autoeventwireup="true" inherits="rfqintro, App_Web_kzgrcpjv" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
    <title>RV Insurance Quote | Blue Sky RV Insurance</title>
	<meta http-equiv="Keywords" name="Keywords" content="RV Insurance Quote, Cheap RV Insurance" />
	<meta http-equiv="Description" name="Description" content="Request an RV Insurance Quote from Blue Sky RV Insurance." />
	<script type="text/javascript">
	    $(document).ready(function () { $('#Tab6').addClass("selected"); });
	    window.onbeforeunload = confirmExit;
	    alertStatus = true;
	    function confirmExit() {
	        if (alertStatus == true) return "All the information that you've entered will be lost.";
	    }
	</script>
    <script src="App.js" type="text/javascript"></script>
<%--    <script type="text/javascript" src="<%= Application["AppPath"] %>/jquery.maskedinput.js"></script>
    <script type="text/javascript">
        jQuery(function ($) {
            $(".date").mask("99/99/9999");
            $(".phone").mask("999-999-9999");
        });
    </script>--%>
  
   <%-- <script type="text/javascript">
        function hearAbout(val) {
            var element = document.getElementById('about');
            if (val == ' ' || val == 'Other')
                element.style.display = 'block';
            else
                element.style.display = 'none';
        }

    </script>  --%>


</asp:Content>
<asp:Content ContentPlaceHolderID="hero" runat="server">
    <section class="full_background clouds_mountain scenic_background">
	    <div class="bl_trans_bg">
            <div class="container">
                <div class="row">
                    <div class="column">
                        <h4>Blue Sky RV Insurance Quote</h4>
                        <p>The Blue Sky RV Insurance program is currently not available for vehicles registered in the following states: AL, AK, CT, DE, FL, HI, KY, MA, MD, ME, MS, NH, NY, RI, VT and WY. If your registration state is listed above please check back soon as we are continuing to expand and add new states to serve you. Coverage is also not available on stationary trailers or park model travel trailers that are registered or garaged in FL, GA, LA, NC, NJ, SC, TX, and VA. </p>
                    </div>
                </div>
            </div>
	    </div>
    </section>
</asp:Content>
<asp:Content ContentPlaceHolderID="main" runat="server">
       <asp:Panel ID="pnlErrors" runat="server" Visible="false">
		<script type="text/javascript">
		    document.getElementById('bodyOveride').style.overflow = 'hidden';
		    document.getElementById('htmlOveride').style.overflow = 'hidden';
		</script>
		<table class="floatingTable u-full-width" border="0">
			<tr>
				<td>
					<div class="floatingPanel">
						<asp:Button ID="cmdFloatClose" runat="server" Text="X" CssClass="common_button button-close" OnClick="cmdFloatClose_Close" OnClientClick="javascript:alertStatus=false" />
						
                        <asp:Panel ID="pnlDefault" runat="server" Visible="true">
							<div class="floatingPanelHeader">
							</div>
							<div class="floatingPanelContentStop">
							</div>
							<div class="floatingPanelContent">
								<div class="floatingPanelContentText">
									<asp:Literal ID="litErrors" runat="server" />
								</div>
							</div>
						</asp:Panel>
						<asp:Panel ID="pnlAlt" runat="server" Visible="false">
							<div class="floatingPanelHeaderAlt">
							</div>
							<div class="floatingPanelContent">
								<div class="floatingPanelContentTextAlt">
									<asp:Literal ID="litNoErrors" runat="server" />
								</div>
							</div>
						</asp:Panel>
					</div>
				</td>
			</tr>
		</table>
	</asp:Panel>
    <div class="row">
        <div class="six columns">
            <label>First Name</label>
            <asp:TextBox placeholder="John" ID="TextBox1" runat="server" MaxLength="30" CssClass="u-full-width rfq_text_field" />
        </div>
        <div class="six columns">
            <label>Last Name</label>
            <asp:TextBox placeholder="Doe" ID="TextBox2" runat="server" MaxLength="30" CssClass="u-full-width rfq_text_field" />
        </div>
    </div>
    <div class="row">
        <div class="twelve columns">
            <label>Email Address</label>
            <asp:TextBox placeholder="test@mailbox.com" ID="TextBox3" runat="server" Type="email" MaxLength="100" CssClass="u-full-width rfq_text_field" />
        </div>
    </div>
    <div class="row">
        <div class="six columns">
            <label>Registration State</label>
            <asp:DropDownList ID="DropDownList2" runat="server"
			CssClass="rfq_text_field u-full-width">
                <asp:ListItem Text="" />
				<asp:ListItem Text="Arizona" Value="AZ" />
				<asp:ListItem Text="Arkansas" Value="AR" />
				<asp:ListItem Text="California" Value="CA" />
				<asp:ListItem Text="Colorado" Value="CO" />
				<%--<asp:ListItem Text="Florida" Value="FL" />--%>
				<asp:ListItem Text="Georgia" Value="GA" />
				<asp:ListItem Text="Idaho" Value="ID" />
				<asp:ListItem Text="Illinois" Value="IL" />
				<asp:ListItem Text="Indiana" Value="IN" />
				<asp:ListItem Text="Iowa" Value="IA" />
				<asp:ListItem Text="Kansas" Value="KS" />
				<asp:ListItem Text="Louisiana" Value="LA" />
				<asp:ListItem Text="Michigan" Value="MI" />
				<asp:ListItem Text="Minnesota" Value="MN" />
				<asp:ListItem Text="Missouri" Value="MO" />
				<asp:ListItem Text="Montana" Value="MT" />
				<asp:ListItem Text="Nebraska" Value="NE" />
				<asp:ListItem Text="New Jersey" Value="NJ" />
				<asp:ListItem Text="New Mexico" Value="NM" />
				<asp:ListItem Text="North Carolina" Value="NC" />
				<asp:ListItem Text="North Dakota" Value="ND" />
				<asp:ListItem Text="Ohio" Value="OH" />
				<asp:ListItem Text="Oklahoma" Value="OK" />
				<asp:ListItem Text="Oregon" Value="OR" />
				<asp:ListItem Text="Pennsylvania" Value="PA" />
				<asp:ListItem Text="South Carolina" Value="SC" />
				<asp:ListItem Text="South Dakota" Value="SD" />
				<asp:ListItem Text="Tennessee" Value="TN" />
				<asp:ListItem Text="Texas" Value="TX" />
				<asp:ListItem Text="Utah" Value="UT" />
				<asp:ListItem Text="Virginia" Value="VA" />
				<asp:ListItem Text="Washington" Value="WA" />
				<asp:ListItem Text="Wisconsin" Value="WI" />
			</asp:DropDownList>
        </div>
        <div class="six columns">
            <label>Vehicle Type</label>
            <asp:DropDownList ID="ddlVehicle1Type" runat="server" CssClass="rfq_text_field u-full-width" >
			    <asp:ListItem Text="" />
                <asp:ListItem Text="Conventional Motorhome (Class A)" class="1" />
			    <asp:ListItem Text="Mini-Motorhome (Class C)" class="1" />
			    <asp:ListItem Text="Camper Van (Class B)" class="1" />
			    <asp:ListItem Text="Conventional Travel Trailer" class="2" />
			    <asp:ListItem Text="Fifth Wheel Camper" class="2" />
			    <asp:ListItem Text="Pop Up Camper" class="2" />
			    <asp:ListItem Text="Truck Camper" class="2" />
			    <asp:ListItem Text="Bus Conversion" class="1" />
			    <asp:ListItem Text="Medium Duty Tow Vehicle" class="1" />
			    <asp:ListItem Text="Totorhome" class="1" />
			    <asp:ListItem Text="Park Model Travel Trailer" class="2" />
			    <asp:ListItem Text="Utility Trailer" class="2" />
		    </asp:DropDownList>
        </div>
    </div>
    <div class="row">
        <div class="six columns">
            <label class="u-pull-left">Phone Number</label>&nbsp;<em>(Optional)</em>
            <asp:TextBox placeholder="XXX-XXX-XXXX" ID="TextBox4" runat="server" type="tel" MaxLength="30" CssClass="u-full-width" />
        </div>
        <div class="six columns">
        </div>
    </div>
    <p>If you would like one of our sales reps to contact you, click �contact me�. Or, if you would like to complete the online quote request, click �continue�.</p>
    <div>
		<asp:Button ID="cmdContact" runat="server" Text="Contact Me" CssClass="button" OnClientClick="javascript:alertStatus=false" OnClick="cmdContact_Click" />
        <asp:Button ID="cmdNext" runat="server" Text="Continue Online Quote" CssClass="button-primary" OnClientClick="javascript:alertStatus=false" OnClick="cmdNext_Click" />
    </div>
</asp:Content>
