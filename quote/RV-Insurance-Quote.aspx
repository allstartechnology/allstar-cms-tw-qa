<%@ page language="C#" masterpagefile="~/AppQuote.master" autoeventwireup="true" inherits="rfqp, App_Web_kzgrcpjv" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
    <title>RV Insurance Quote | Blue Sky RV Insurance</title>
	<meta http-equiv="Keywords" name="Keywords" content="RV Insurance Quote, Cheap RV Insurance" />
	<meta http-equiv="Description" name="Description" content="Request an RV Insurance Quote from Blue Sky RV Insurance." />
	<script type="text/javascript">
	    $(document).ready(function () { $('#Tab6').addClass("selected"); });
	    window.onbeforeunload = confirmExit;
	    alertStatus = true;
	    function confirmExit() {
	        if (alertStatus == true) return "All the information that you've entered will be lost.";
	    }
	</script>
    <script src="App.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%= Application["AppPath"] %>/jquery.maskedinput.js"></script>
    <script type="text/javascript">
        jQuery(function ($) {
            $(".date").mask("99/99/9999");
            $(".phone").mask("999-999-9999");
        });
    </script>
    <!-- Start Quantcast Tag -->
    <script type="text/javascript">
        var _qevents = _qevents || [];

        (function () {
            var elem = document.createElement('script');
            elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
            elem.async = true;
            elem.type = "text/javascript";
            var scpt = document.getElementsByTagName('script')[0];
            scpt.parentNode.insertBefore(elem, scpt);
        })();

        _qevents.push(
        { qacct: "p-VypNfW3as3_Ed", labels: "_fp.event.Quote Form" }
        );
    </script>
    <noscript>
    <img src="//pixel.quantserve.com/pixel/p-VypNfW3as3_Ed.gif?labels=_fp.event.Quote+Form" style="display: none;" border="0" height="1" width="1" alt="Quantcast"/>
    </noscript>
    <!-- End Quantcast tag -->


   <%-- <script type="text/javascript">
        function hearAbout(val) {
            var element = document.getElementById('about');
            if (val == ' ' || val == 'Other')
                element.style.display = 'block';
            else
                element.style.display = 'none';
        }

    </script>  --%>


</asp:Content>
<asp:Content ContentPlaceHolderID="hero" runat="server">
    <section class="full_background clouds_mountain scenic_background">
	    <div class="bl_trans_bg">
            <div class="container">
                <div class="row">
                    <div class="column">
                        <h4>Blue Sky RV Insurance Quote</h4>
                        <p>The Blue Sky RV Insurance program is currently not available for vehicles registered in the following states: AL, AK, CT, DE, FL, HI, KY, MA, MD, ME, MS, NH, NY, RI, VT and WY. If your registration state is listed above please check back soon as we are continuing to expand and add new states to serve you. Coverage is also not available on stationary trailers or park model travel trailers that are registered or garaged in FL, GA, LA, NC, NJ, SC, TX, and VA. </p>
                    </div>
                </div>
            </div>
	    </div>
    </section>
</asp:Content>
<asp:Content ContentPlaceHolderID="main" runat="server">
        <asp:Panel ID="pnlErrors" runat="server" Visible="false">
			<script type="text/javascript">
			    document.getElementById('bodyOveride').style.overflow = 'hidden';
			    document.getElementById('htmlOveride').style.overflow = 'hidden';
			</script>
			<table class="floatingTable u-full-width" border="0">
				<tr>
					<td>
						<div class="floatingPanel">
                            <asp:Button ID="cmdFloatClose" runat="server" Text="X" CssClass="common_button" OnClick="cmdFloatClose_Close" OnClientClick="javascript:alertStatus=false" />
							<asp:Panel ID="pnlDefault" runat="server" Visible="true">
								<div class="floatingPanelHeader">
								</div>
								<div class="floatingPanelContentStop">
								</div>
								<div class="floatingPanelContent">
									<div class="floatingPanelContentText">
										<asp:Literal ID="litErrors" runat="server" />
									</div>
								</div>
							</asp:Panel>
							<asp:Panel ID="pnlAlt" runat="server" Visible="false">
								<div class="floatingPanelHeaderAlt">
								</div>
								<div class="floatingPanelContent">
									<div class="floatingPanelContentTextAlt">
										<asp:Literal ID="litNoErrors" runat="server" />
									</div>
								</div>
							</asp:Panel>
						</div>
					</td>
				</tr>
			</table>
			<div class="floatingCore">
			</div>
		</asp:Panel>
        <asp:Panel ID="pnlPanel1" runat="server" Visible="true">
            <div class="row">
                
                <p class="header_text_disclaimer">Motorized vehicle types must have a current value between $20,000 and $2,000,000 to be eligible for coverage
                in our program. Towable unit types must have a current value between $5,000 and $250,000 to be eligible for
                coverage in our program.</p>
                <hr />
		        <h4>Applicant Information</h4>
            </div>
            <div class="row">
                <div class="six columns">
                    <label>How Did you Hear About Us?</label>
                    <asp:DropDownList ID="DropDownListHear" runat="server" CssClass="u-full-width rfq_text_field"><%-- onchange='hearAbout(this.value);'>--%>
                        <%-- <asp:ListItem Text="" />
				        <asp:ListItem Text="Magazine Advertisement" />
				        <asp:ListItem Text="Google Search" />
				        <asp:ListItem Text="Referred by Current Customer" />
                        <asp:ListItem Text="I am a Prior Customer" />
                        <asp:ListItem Text="Internet Advertisement" />
				        <asp:ListItem Text="Other" />--%>
			        </asp:DropDownList>
                </div>
                <div class="six columns">
                    <label>If other, please explain:</label>
                    <asp:TextBox ID="TextBoxHear" runat="server" MaxLength="30" CssClass="u-full-width" />
                </div>
            </div>
            <div class="row">
                <div class="six columns">
                    <label>Referral Code</label>
                    <asp:TextBox ID="TextBoxReferralCode" runat="server" MaxLength="30" CssClass="u-full-width" />
                </div>
                <div class="six columns">

                </div>
            </div>
            <div class="row">
                <div class="three columns">
                    <label>First Name</label>
                    <asp:TextBox ID="txtNameFirst" placeholder="John" runat="server" MaxLength="15" CssClass="u-full-width rfq_text_field" />
                </div>
                <div class="two columns">
                    <label>Middle Inital</label>
                    <asp:TextBox ID="txtNameMiddle" runat="server" MaxLength="1" CssClass="u-full-width rfq_text_field" />
                </div>
                <div class="six columns">
                    <label>Last Name</label>
                    <asp:TextBox ID="txtNameLast" placeholder="Doe" runat="server" MaxLength="15" CssClass="u-full-width rfq_text_field" />
                </div>
            </div>
             <div class="row">
                <div class="six columns">
                    <label>Phone Number</label>
                    <asp:TextBox ID="txtPhoneFax" placeholder="XXX-XXX-XXXX" Mask="999-999-9999" MaskType="Number" runat="server" CssClass="u-full-width rfq_text_field phone" />
                </div>
                <div class="six columns">
                    <label class="u-pull-left">Last 4 of SSN</label>&nbsp;(<asp:LinkButton ID="lnkWhatsThisSSN" runat="server" OnClick="lnkWhatsThisSSN_Click" OnClientClick="javascript:alertStatus=false">What's This?</asp:LinkButton>)
                    <asp:TextBox ID="TextBoxSSN" placeholder="XXXX" runat="server" ClearMaskOnLostFocus="false" CssClass="u-full-width rfq_text_field" MaskType="Number" MaxLength="4"/>
                </div>
            </div>
             <div class="row">
                <div class="six columns" id="email">
                    <label>Email Address</label>
                    <asp:TextBox placeholder="test@mailbox.com" ID="txtEmail" runat="server" CssClass="u-full-width rfq_text_field" MaxLength="50" />
                </div>
                <div class="six columns" id="email">
                    <label>Confirm Email Address</label>
                    <asp:TextBox ID="txtEmailConfirm" runat="server" MaxLength="50" CssClass="u-full-width rfq_text_field" />
                </div>
            </div>
             <div class="row">
                <div class="six columns">
                    <label>What is the ownership type for the RV?</label>
                    <asp:DropDownList ID="ddlOwnership" runat="server" CssClass="u-full-width rfq_text_field">
				        <asp:ListItem Text="Individual" />
				        <asp:ListItem Text="Corporation" />
				        <asp:ListItem Text="Limited Liability Company" />
				        <asp:ListItem Text="Partnership" />
				        <asp:ListItem Text="Trust" />
			        </asp:DropDownList>
                </div>
                <div class="six columns">
                    <label>If ownership is not Individual, what is the name of the entity?</label>
                    <asp:TextBox ID="txtOwnershipEntity" runat="server" MaxLength="50" CssClass="u-full-width rfq_text_field" />
                </div>
            </div>
             <div class="row">
                <div class="six columns">
                    <label>What is your current policy Expiration Date?</label>
                    <asp:TextBox ID="TextBoxExpirationDate" placeholder="XX/XX/XXXX" runat="server" MaxLength="50" CssClass="u-full-width rfq_text_field date datepicker" />
                </div>
                <div class="six columns">
                    <a id="bbblink" class="ruvtbum" href="http://www.bbb.org/akron/business-reviews/insurance-agency/blue-sky-rv-insurance-in-akron-oh-90020867#bbblogo" title="Blue Sky RV Insurance, Insurance Agency, Akron, OH" style="display: block;position: relative;overflow: hidden; width: 60px; height: 108px; margin: 0px; padding: 0px;"><img style="padding: 0px; border: none;" id="bbblinkimg" src="http://seal-akron.bbb.org/logo/ruvtbum/blue-sky-rv-insurance-90020867.png" width="120" height="108" alt="Blue Sky RV Insurance, Insurance Agency, Akron, OH" /></a><script type="text/javascript">var bbbprotocol = (("https:" == document.location.protocol) ? "https://" : "http://"); document.write(unescape("%3Cscript src='" + bbbprotocol + 'seal-akron.bbb.org' + unescape('%2Flogo%2Fblue-sky-rv-insurance-90020867.js') + "' type='text/javascript'%3E%3C/script%3E"));</script>
								
                </div>
            </div>
            <hr />
            <h4>Home Mailing Address</h4>
             <div class="row">
                <div class="nine columns">
                    <label>Street Address</label>
                    <asp:TextBox ID="txtAddress1"  runat="server" MaxLength="30" CssClass="u-full-width rfq_text_field" />
                </div>
                <div class="three columns">
                    <label>Apt./Suite #</label>
                    <asp:TextBox ID="txtAddress2" runat="server" MaxLength="30" CssClass="u-full-width rfq_text_field" />
                </div>
            </div>
            <div class="row">
                <div class="four columns">
                    <label>City</label>
                    <asp:TextBox ID="txtAddressCity" runat="server" MaxLength="30" CssClass="u-full-width rfq_text_field" />
                </div>
                <div class="three columns">
                    <label>State</label>
                    <asp:DropDownList ID="ddlAddressState" runat="server" CssClass="u-full-width rfq_text_field">
				        <asp:ListItem Text="Select" />
				        <%--asp:ListItem Text="Alabama" Value="AL" /--%>
				        <%--asp:ListItem Text="Alaska" Value="AK" /--%>
				        <asp:ListItem Text="Arizona" Value="AZ" />
				        <asp:ListItem Text="Arkansas" Value="AR" />
				        <asp:ListItem Text="California" Value="CA" />
				        <asp:ListItem Text="Colorado" Value="CO" />
				        <%--asp:ListItem Text="Connecticut" Value="CT" /--%>
				        <%--asp:ListItem Text="Delaware" Value="DE" /--%>
				        <%--<asp:ListItem Text="Florida" Value="FL" />--%>
				        <asp:ListItem Text="Georgia" Value="GA" />
				        <%--asp:ListItem Text="Hawaii" Value="HI" /--%>
				        <asp:ListItem Text="Idaho" Value="ID" />
				        <asp:ListItem Text="Illinois" Value="IL" />
				        <asp:ListItem Text="Indiana" Value="IN" />
				        <asp:ListItem Text="Iowa" Value="IA" />
				        <asp:ListItem Text="Kansas" Value="KS" />
				        <%--asp:ListItem Text="Kentucky" Value="KY" /--%>
				        <asp:ListItem Text="Louisiana" Value="LA" />
				        <%--asp:ListItem Text="Maine" Value="ME" /--%>
				        <%--asp:ListItem Text="Maryland" Value="MD" /--%>
				        <%--asp:ListItem Text="Massachusetts" Value="MA" /--%>
				        <asp:ListItem Text="Michigan" Value="MI" />
				        <asp:ListItem Text="Minnesota" Value="MN" />
				        <%--asp:ListItem Text="Mississippi" Value="MS" /--%>
				        <asp:ListItem Text="Missouri" Value="MO" />
				        <asp:ListItem Text="Montana" Value="MT" />
				        <asp:ListItem Text="Nebraska" Value="NE" />
				        <asp:ListItem Text="Nevada" Value="NV" />
				        <asp:ListItem Text="New Hampshire" Value="NH" />
				        <asp:ListItem Text="New Jersey" Value="NJ" />
				        <asp:ListItem Text="New Mexico" Value="NM" />
				        <%--asp:ListItem Text="New York" Value="NY" /--%>
				        <asp:ListItem Text="North Carolina" Value="NC" />
				        <asp:ListItem Text="North Dakota" Value="ND" />
				        <asp:ListItem Text="Ohio" Value="OH" />
				        <asp:ListItem Text="Oklahoma" Value="OK" />
				        <asp:ListItem Text="Oregon" Value="OR" />
				        <asp:ListItem Text="Pennsylvania" Value="PA" />
				        <%--asp:ListItem Text="Rhode Island" Value="RI" /--%>
				        <asp:ListItem Text="South Carolina" Value="SC" />
				        <asp:ListItem Text="South Dakota" Value="SD" />
				        <asp:ListItem Text="Tennessee" Value="TN" />
				        <asp:ListItem Text="Texas" Value="TX" />
				        <asp:ListItem Text="Utah" Value="UT" />
				        <%--asp:ListItem Text="Vermont" Value="VT" /--%>
				        <asp:ListItem Text="Virginia" Value="VA" />
				        <asp:ListItem Text="Washington" Value="WA" />
				        <%--asp:ListItem Text="Washington DC" Value="DC" /--%>
				        <asp:ListItem Text="West Virginia" Value="WV" />
				        <asp:ListItem Text="Wisconsin" Value="WI" />
				        <%--asp:ListItem Text="Wyoming" Value="WY" /--%>
			        </asp:DropDownList>
                </div>
                <div class="five columns">
                    <label>Zip</label>
                    <asp:TextBox ID="txtAddressZip" runat="server" MaxLength="10" CssClass="u-full-width rfq_text_field" />
                </div>
            </div>
            <div class="row">
                <div class="six columns">
                    <label>Are you a homeowner?</label>
                    <asp:DropDownList ID="DropDownListHomeOwner" runat="server" CssClass="u-full-width rfq_text_field">
				        <asp:ListItem Text="" />
                        <asp:ListItem Text="Yes" Value="Y" />
				        <asp:ListItem Text="No" Value="N"/>
			        </asp:DropDownList>
                </div>
                <div class="six columns">

                </div>
            </div>
            <hr />
            <h4>Policy Information</h4>
            <div class="row">
                <div class="twelve columns">
                    <label>Liability Limits Requested for Motorized Vehicles</label>
                     <asp:DropDownList ID="ddlLiabiltiyLimitsRequested" runat="server" CssClass="rfq_text_field">
				        <asp:ListItem Text="" />
				        <asp:ListItem Text="50/100/25" />
				        <asp:ListItem Text="100/300/50" />
                        <asp:ListItem Text="250/500/100" />
				        <asp:ListItem Text="300 CSL" />
				        <asp:ListItem Text="500 CSL" />
			        </asp:DropDownList>
                </div>
            </div>
            <label class="u-pull-left">Are you a member of an RV Association?</label>&nbsp;(<asp:LinkButton ID="lnkWhatsThisRVAss" runat="server" OnClick="lnkWhatsThisRVAss_Click" OnClientClick="javascript:alertStatus=false">What's This?</asp:LinkButton>)
            <div class="row">
                <div class="twelve columns">
                     <asp:DropDownList ID="ddlRVAssociation" runat="server" CssClass="rfq_text_field">
				        <asp:ListItem Text="" />
				        <asp:ListItem Text="Yes" />
				        <asp:ListItem Text="No" />
			        </asp:DropDownList>
                </div>
            </div>
            <label class="u-pull-left">Are you a member of an RV Manufacturer&#8217;s Club?&nbsp;</label>&nbsp;(<asp:LinkButton ID="lnkWhatsThisRvMfrClub" runat="server" OnClick="lnkWhatsThisRvMfrClub_Click" OnClientClick="javascript:alertStatus=false">What's This?</asp:LinkButton>)
            <div class="row">
                <div class="twelve columns">
                    <asp:DropDownList ID="ddlRVManufacturersClub" runat="server" CssClass="rfq_text_field">
				        <asp:ListItem Text="" />
				        <asp:ListItem Text="Yes" />
				        <asp:ListItem Text="No" />
			        </asp:DropDownList>
                </div>
            </div>
            <label class="u-pull-left">Is any RV to be insured owned by two or more individuals that are not husband and wife?</label>&nbsp;(<asp:LinkButton ID="lnkWhatsThisTwo" runat="server" OnClick="lnkWhatsThisTwo_Click" OnClientClick="javascript:alertStatus=false">What's This?</asp:LinkButton>)
            <div class="row">
                <div class="twelve columns">
                    <asp:DropDownList ID="ddlRVDualOwnership" runat="server" CssClass="rfq_text_field">
				        <asp:ListItem Text="" />
				        <asp:ListItem Text="Yes" />
				        <asp:ListItem Text="No" />
			        </asp:DropDownList>
                </div>
            </div>
            <label class="u-pull-left">Is any RV to be insured used for commercial or business purposes?</label>&nbsp;(<asp:LinkButton ID="lnkWhatsThisPurpose" runat="server" OnClick="lnkWhatsThisPurpose_Click" OnClientClick="javascript:alertStatus=false">What's This?</asp:LinkButton>)
            <div class="row">
                <div class="twelve columns">
                    <asp:DropDownList ID="ddlCommercialPurposes" runat="server" CssClass="rfq_text_field">
				        <asp:ListItem Text="" />
				        <asp:ListItem Text="Yes" />
				        <asp:ListItem Text="No" />
			        </asp:DropDownList>
                </div>
            </div>
            <label class="u-pull-left">Is any RV to be insured used as a primary residence more than five months out of the year?</label>&nbsp;(<asp:LinkButton ID="lnkWhatsRvYear" runat="server" OnClick="lnkWhatsRvYear_Click" OnClientClick="javascript:alertStatus=false">What's This?</asp:LinkButton>)
            <div class="row">
                <div class="twelve columns">
                    <asp:DropDownList ID="ddlPrimaryResidence" runat="server" CssClass="rfq_text_field">
				        <asp:ListItem Text="" />
				        <asp:ListItem Text="Yes" />
				        <asp:ListItem Text="No" />
			        </asp:DropDownList>
                </div>
            </div>
    </asp:Panel>
	<asp:Panel ID="pnlControls" runat="server" Visible="true">
	    <asp:Button ID="cmdNext" runat="server" Text="Next" CssClass="button-primary" OnClick="cmdNext_Click" OnClientClick="javascript:alertStatus=false" />
	</asp:Panel>
    <p class="header_text_disclaimer">In connection with this application for insurance we will collect information from consumer reporting agencies such as driving records. We may also obtain an insurance credit score. We may use a third party in connection with the development of your insurance credit score. By submitting this request for quote you are providing permission for us to obtain driving record and an insurance credit score.</p>
	<script type="text/javascript" src="moment.js"></script>
	<script type="text/javascript" src="pikaday.js"></script>
	<script type="text/javascript" src="pikadayJQ.js"></script>
    <script>

        var $datepicker = $('.datepicker').pikaday({
            format: 'L',
            disableWeekends: true,
            minDate: new Date(),
            maxDate: new Date(2020 - 12 - 31),
            yearRange: [2015, 2020]
        });
        // chain a few methods for the first datepicker, jQuery style!
        //$datepicker.pikaday('show').pikaday('nextMonth');

    </script>
</asp:Content>
