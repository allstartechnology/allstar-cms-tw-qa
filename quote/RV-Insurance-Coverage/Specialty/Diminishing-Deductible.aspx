﻿<%@ Page Language="C#" MasterPageFile="_Specialty.master" %>

<asp:content ID="Content1" contentplaceholderid="head" runat="server">
	<title>RV Insurance | Diminishing Deductible | Blue Sky RV Insurance</title>
	<meta http-equiv="Keywords" name="Keywords" content="Specialty RV insurance, diminishing deductible">
	<meta http-equiv="Description" name="Description" content="Diminishing Deductible coverage from Blue Sky RV Insurance saves you money by reducing your Comprehensive and Collision deductible for every loss-free year you’re covered.  Specialty RV Insurance provided to the RV enthusiast.">
	<style type="text/css">
		div.content { height:780px; }
	</style>
	<script type="text/javascript">
		$(document).ready(function() { $('a#Diminishing-Deductible').addClass("Active"); });
  </script>
</asp:content>

<asp:Content ID="Content3" ContentPlaceHolderID="background_image" runat="server">
	<img src="<%= Application["AppPath"] %>/images/specialty_rv_insurance_bg.jpg" alt="" />
</asp:Content>
		
<asp:Content ID="Content2" ContentPlaceHolderID="main_title" runat="server">
	Diminishing Deductible<br />RV Insurance
</asp:Content>
			
<asp:Content ID="Content4" ContentPlaceHolderID="main_details" runat="server">
	<p>Diminishing Deductible reduces your Comprehensive and Collision deductible by 25% for every loss-free year. Diminishing deductible from Blue Sky RV Insurance ensures that at the end of 4 years of loss-free experience, your deductible is just $0 in the event of a claim.</p>
	<p>Blue Sky RV Insurance also offers a <a href="Diminishing-Deductible-Express.aspx">diminishing deductible Express</a> Option that earns your rewards in half the time!</p>
</asp:content>