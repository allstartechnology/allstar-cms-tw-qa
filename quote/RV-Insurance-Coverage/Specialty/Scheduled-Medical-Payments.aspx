﻿<%@ Page Language="C#" MasterPageFile="_Specialty.master" %>

<asp:content ID="Content1" contentplaceholderid="head" runat="server">
	<title>RV Insurance | Scheduled Medical Payments | Blue Sky RV Insurance</title>
	<meta http-equiv="Keywords" name="Keywords" content="Specialty RV insurance, scheduled medical payments">
	<meta http-equiv="Description" name="Description" content="The Blue Sky RV Insurance program offers Scheduled Medical Payments to provide coverage for scheduled injuries that occur in non-vehicle accidents.">
	<style type="text/css">
		div.content { height: 1010px; }
	</style>
	<script type="text/javascript">
		$(document).ready(function() { $('a#Scheduled-Medical-Payments').addClass("Active"); });
  </script>
</asp:content>

<asp:Content ID="Content3" ContentPlaceHolderID="background_image" runat="server">
	<img src="<%= Application["AppPath"] %>/images/Scheduled_Medical_Payments_coverages_bg.jpg" alt="Specialty RV Insurance – Full Timer RV Insurance" />
</asp:Content>
		
<asp:Content ID="Content2" ContentPlaceHolderID="main_title" runat="server">
	Scheduled Medical Payments RV Insurance
</asp:Content>
			
<asp:Content ID="Content4" ContentPlaceHolderID="main_details" runat="server">
	<p>Scheduled Medical Payments provide coverage for scheduled injuries that occur in non-vehicle accidents. Scheduled Medical Payments provides scheduled medical payments to the insured and family members of the insured in the event of a non-vehicle accident.</p>
	<h2>Scheduled Medical Payments – Benefits</h2>
	<img width="420px" style="margin-bottom:20px" src="<%= Application["AppPath"] %>/images/Scheduled_Medical_Payments.JPG" alt="Scheduled Medical Payments" />
</asp:Content>