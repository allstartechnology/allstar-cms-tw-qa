<%@ Page Language="C#" MasterPageFile="_Specialty.master" %>

<asp:content ID="Content1" contentplaceholderid="head" runat="server">
	<title>RV Insurance | Total Loss Replacement | Blue Sky RV Insurance</title>
	<meta http-equiv="Keywords" name="Keywords" content="Specialty RV insurance, total loss replacement">
	<meta http-equiv="Description" name="Description" content="Total Loss Replacement with Blue Sky RV Insurance has your recreational vehicle covered. At Blue Sky we will replace your RV or provide the purchase price under our Total Loss Replacement coverage. Contact us for details.">
	<style type="text/css">
		div.content { height: 850px; }
	</style>
	<script type="text/javascript">
		$(document).ready(function() { $('a#Total-Loss-Replacement-Coverage').addClass("Active"); });
  </script>
</asp:content>

<asp:Content ID="Content3" ContentPlaceHolderID="background_image" runat="server">
	<img src="<%= Application["AppPath"] %>/images/specialty_rv_insurance_bg.jpg" alt="" />
</asp:Content>
		
<asp:Content ID="Content2" ContentPlaceHolderID="main_title" runat="server">
	Total Loss Replacement<br />RV Insurance
</asp:Content>
			
<asp:Content ID="Content4" ContentPlaceHolderID="main_details" runat="server">
	<p>If your recreational vehicle suffers a total loss, Total Loss Replacement will replace your RV with a new one for the first 5 model years, and provide the purchase price toward a new unit after five model years. See how Total Loss Replacement with Blue Sky RV Insurance differs from that of typical RV insurance.</p>
	<p>Total Loss Replacement is available to original owners of the RV during the first 5 model years. <b>Total Loss Replacement Coverage from Blue Sky will allow you to pick your choice of manufacturer, floor plan, and color in the event of a Total Loss Replacement.</b></p>
</asp:Content>