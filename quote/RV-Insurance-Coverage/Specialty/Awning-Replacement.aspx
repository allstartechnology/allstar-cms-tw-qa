﻿<%@ Page Language="C#" MasterPageFile="_Specialty.master" %>

<asp:content ID="Content1" contentplaceholderid="head" runat="server">
	<title>RV Insurance | Awning Replacement | Blue Sky RV Insurance</title>
	<meta http-equiv="Keywords" name="Keywords" content="Specialty RV insurance, Awning Replacement">
	<meta http-equiv="Description" name="Description" content="Blue Sky RV Insurance clients who purchase Total Loss Replacement coverage are also eligible for additional Awning Replacement, a specialty RV insurance coverage.">
	<style type="text/css">
		div.content { height: 760px; }
	</style>
	<script type="text/javascript">
		$(document).ready(function() { $('a#Awning-Replacement').addClass("Active"); });
  </script>
</asp:content>

<asp:Content ID="Content3" ContentPlaceHolderID="background_image" runat="server">
	<img src="<%= Application["AppPath"] %>/images/specialty_rv_insurance_bg.jpg" alt="" />
</asp:Content>
		
<asp:Content ID="Content2" ContentPlaceHolderID="main_title" runat="server">
	Awning Replacement RV Insurance
</asp:Content>
			
<asp:Content ID="Content4" ContentPlaceHolderID="main_details" runat="server">
	<p>Purchasing Awning Replacement modifies the loss settlement provisions so that no depreciation applies to a partial loss of permanently attached awnings. Awning Replacement is available if <a href="Total-Loss-Replacement-Coverage.aspx">Total Loss Replacement</a> coverage has also been purchased. To provide complete protection against depreciation for all component parts, the Blue Sky RV Insurance program also offers a <a href="Depreciation-Free.aspx">Depreciation Free RV Insurance option.</a></p>
</asp:content>