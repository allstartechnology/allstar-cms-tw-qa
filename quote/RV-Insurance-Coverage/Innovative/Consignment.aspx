<%@ Page Language="C#" MasterPageFile="_Innovative.master" %>

<asp:content ID="Content1" contentplaceholderid="head" runat="server">
	<title>RV Insurance | Consignment Coverage | Blue Sky RV Insurance</title>
	<meta http-equiv="Keywords" name="Keywords" content="RV insurance, innovative RV insurance, consignment coverage">
	<meta http-equiv="Description" name="Description" content="The Blue Sky RV Insurance program protects recreational vehicles while being sold by offering consignment coverage.  This innovative RV insurance coverage option is available through Blue Sky.">
	<style type="text/css">
		div.content { height: 670px; }
	</style>
	<script type="text/javascript">
		$(document).ready(function() { $('a#Consignment').addClass("Active"); });
  </script>
</asp:content>

<asp:Content ID="Content3" ContentPlaceHolderID="background_image" runat="server">
	<img src="<%= Application["AppPath"] %>/images/Consignment_coverages_bg.jpg" alt="Consignment Coverage" />
</asp:Content>
		
<asp:Content ID="Content2" ContentPlaceHolderID="main_title" runat="server">
	Consignment Coverage RV Insurance
</asp:Content>
			
<asp:Content ID="Content4" ContentPlaceHolderID="main_details" runat="server">
	<p>Most specialty RV insurance policies exclude coverage for your recreational vehicle when your RV is on a consignment lot or otherwise held for sale by a third party.  With Consignment Coverage offered by the Blue Sky RV Insurance program, you have the freedom to have your vehicle in the possession of a RV dealer for sale under consignment. During tough economic times we understand that you might need to make a difficult decision to sell your RV. There's no need to worry about a loss while it is on consignment as long as you add Blue Sky's consignment coverage to your Blue Sky RV
Insurance policy. </p>
</asp:Content>