<%@ Page Language="C#" MasterPageFile="_Innovative.master" %>

<asp:content ID="Content1" contentplaceholderid="head" runat="server">
	<title>RV Insurance | Total Loss Replacement | Blue Sky RV Insurance</title>
  <meta http-equiv="Keywords" name="Keywords" content="RV insurance, innovative RV insurance, total loss replacement" />
	<meta http-equiv="Description" name="Description" content="Blue Sky RV Insurance has developed Innovative RV insurance coverage options including a new way to offer Total Loss Replacement. This exciting new RV insurance option is available for motorhomes, travel trailers, and fifth wheels." />
	<style type="text/css">
		div.content { height: 800px; }
	</style>
	<script type="text/javascript">
		$(document).ready(function() { $('a#TotalLossReplacement').addClass("Active");});
  </script>
</asp:content>

<asp:Content ID="Content3" ContentPlaceHolderID="background_image" runat="server">
	<img src="<%= Application["AppPath"] %>/images/totalloss_coverages_bg.jpg" alt="" />
</asp:Content>
		
<asp:Content ID="Content2" ContentPlaceHolderID="main_title" runat="server">
	Total Loss Replacement RV Insurance
</asp:Content>
			
<asp:Content ID="Content4" ContentPlaceHolderID="main_details" runat="server">
	<p>You have taken great time and pride in selecting and personalizing your recreational vehicle to make sure it is just right for you and your family. In the unfortunate event of a total loss to your RV, don&#8217;t let a claims adjuster determine which replacement vehicle is right for you.</p>
	<p>Unlike other specialty RV insurance, with Blue Sky Total Loss Replacement the  choice of manufacturer, floor plan, and color is completely up to you!</p>
	<h2>RV Insurance Claim Example</h2>
	<p>If you buy a new recreational vehicle for $100,000 and 3 years later it is involved in a total loss, Total Loss Replacement with the Blue Sky RV Insurance Program would pay up to $123,296 toward the purchase of a new RV.  Unlike other specialty RV insurance programs, you get to choose the new manufacturer, floor plan, and color!</p>
</asp:Content>
