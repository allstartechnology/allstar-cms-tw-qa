﻿<%@ Page Language="C#" MasterPageFile="_Innovative.master" %>

<asp:content ID="Content1" contentplaceholderid="head" runat="server">
	<title>RV Insurance | Depreciation Free | Blue Sky RV Insurance</title>
  <meta http-equiv="Keywords" name="Keywords" content="RV insurance, innovative RV insurance, depreciation free RV insurance" />
	<meta http-equiv="Description" name="Description" content="The Blue Sky RV Insurance program offers an innovative RV insurance coverage known as the Depreciation Free RV Insurance coverage option. " />
	<style type="text/css">
		div.content { height: 950px; }
	</style>
	<script type="text/javascript">
	    $(document).ready(function () { $('a#DepreciationFree').addClass("Active"); });
  </script>
</asp:content>

<asp:Content ID="Content3" ContentPlaceHolderID="background_image" runat="server">
	<img src="<%= Application["AppPath"] %>/images/Depreciation_free_coverages_bg.jpg" alt="" />
</asp:Content>
		
<asp:Content ID="Content2" ContentPlaceHolderID="main_title" runat="server">
	Depreciation Free RV Insurance
</asp:Content>
			
<asp:Content ID="Content4" ContentPlaceHolderID="main_details" runat="server">
	<p>If you carry Total Loss Replacement RV insurance, you have taken the first step toward insuring your recreational vehicle against depreciation. However, Total Loss Replacement only provides protection against depreciation in the event of a total loss and does not provide replacement cost on partial losses.</p>
	<p><b>Blue Sky is proud to offer the industry’s first depreciation-free policy!</b> <br />The Depreciation Free RV Insurance option provides replacement cost coverage for losses that are typically settled on an actual cash value basis by other RV insurance carriers such as damage to tires, awnings, screen doors, electronics, and more.</p>
	<h2>RV Insurance Claim Example</h2>
	<p>Imagine you run over an object on the highway, damaging 2 tires. At the time of the accident, the treads on the tires are 50% worn and the cost to replace the tires is $600 each. If you had a $500 deductible your standard insurance would pay just $100 and you would be out of pocket $1,100!</p>
	<p>The Depreciation Free Insurance coverage option with the Blue Sky RV Insurance program does not penalize you for depreciation. So the $1,200 cost to replace the tires would be reduced by the $500 deductible and pay the remaining $700 for the loss &#8211; a savings of $600 over traditional RV insurance!</p>
</asp:Content>
