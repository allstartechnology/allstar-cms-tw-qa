﻿<%@ Page Language="C#" MasterPageFile="_Section.master" %>

<asp:content contentplaceholderid="head" runat="server">
	<title>RV Roadside Assistance | Blue Sky RV Insurance</title>
  <meta http-equiv="Keywords" name="Keywords" content="Blue Sky RV Motor Club, Nation’s Safe Drivers, Roadside Assistance, Blue Sky RV Insurance">
	<meta http-equiv="Description" name="Description" content="Blue Sky RV Insurance offers its customers the Blue Sky RV Motor Club – a 24 hour emergency roadside assistance program.">
	<style type="text/css">
		div.content { height: 1040px; }
		div#Disclaimer { display: none; }
		div.coverages_header { width: 430px; line-height: 25px; }
		div.text h1 { margin: 0; }
		div.text h2 { margin: 0; }
	</style>
</asp:content>

<asp:Content ID="Content3" ContentPlaceHolderID="background_image" runat="server">
		<img src="<%= Application["AppPath"] %>/images/rv-roadside-assistance_bg.jpg" alt="" />
</asp:Content>
		
<asp:Content ID="Content2" ContentPlaceHolderID="main_title" runat="server">
		24 Hour RV Roadside Assistance
</asp:Content>
			
<asp:Content ID="Content4" ContentPlaceHolderID="main_details" runat="server">
			<p>Blue Sky RV Insurance is proud to offer its customers the Blue Sky RV Motor Club – a 24 hour emergency roadside assistance program managed by Nation Safe Drivers.  Services are available throughout the 50 United States, Puerto Rico and Canada.</p>
			<p>When a member’s auto is disabled, a service vehicle will be dispatched to your aid.  All road services are paid for in total by your membership, up to the limit of towing expenses selected by the member.  The membership will cover only 1 service in a 72-hour period and is limited to 5 services per membership term.  If for any reason, road service cannot be dispatched, you may obtain covered services on your own and be reimbursed up to $300.</p>
			<h2>Emergency RV Roadside Assistance Services</h2>
			<div class="coverages_item">
				<ul>
					<li><b>Mechanical First Aid:</b> Any service requiring a minor adjustment (exclusive of parts) to enable a disabled vehicle to proceed under its own power</li>
					<li><b>Tire Service:</b> the changing of a flat tire with a member provided, good inflatable spare</li>
					<li><b>Battery Service:</b> Attempting to start a vehicle with a booster battery</li>
					<li><b>Delivery Service:</b> Delivery of an emergency supply of gasoline, oil or water and other accessories and supplies as may be required and available.  Cost of materials delivered shall be paid by the member and payment is expected at the time service is rendered.</li>
					<li><b>Towing Service:</b> When a vehicle cannot be started, or is unsafe to operate, it may be towed to the nearest destination qualified to make repairs on your vehicle subject to the limit of towing expenses selected by the member.</li>
					<li><b>Lockout Service:</b> We will assist in gaining entry to your locked vehicle in the event the keys are lost or locked inside.</li>
					<li><b>Map Service:</b> Your membership allows you to request and receive specially prepared maps for travel.</li>
					<li><b>Theft Reward:</b> The Blue Sky RV Motor Club will pay a person (excluding a member’s family or relatives) $500 for information leading to the arrest and conviction of a person for the theft of a member’s vehicle or tagged valuable articles.</li>
				</ul>
			</div>
</asp:Content>