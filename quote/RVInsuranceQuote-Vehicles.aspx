<%@ page language="C#" masterpagefile="~/AppQuote.master" autoeventwireup="true" inherits="rfqv, App_Web_kzgrcpjv" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
	<title>RV Insurance Quote | Vehicles | Blue Sky RV Insurance</title>
	<meta http-equiv="Keywords" name="Keywords" content="RV Insurance Quote, Recreational Vehicle" />
	<meta http-equiv="Description" name="Description" content="Request an RV Insurance Quote from Blue Sky RV Insurance." />
	<script type="text/javascript">
	    $(document).ready(function () { $('#Tab6').addClass("selected"); });
	    window.onbeforeunload = confirmExit;
	    alertStatus = true;
	    function confirmExit() {
	        if (alertStatus == true) return "All the information that you've entered will be lost.";
	    }
	</script>
	<style type="text/css">
		.style3 { width: 300px; }
		.style4 { width: 293px; }
		.style5 { width: 236px; }
		.style6 { height: 15px; }
		.style7 {
			width: 300px;
			height: 15px;
		}
		div.rfq_content { height: 1550px; }
	</style>
    <script type="text/javascript" src="<%= Application["AppPath"] %>/jquery.maskedinput.js"></script>
    <script type="text/javascript">
        jQuery(function ($) {
            $(".date").mask("99/99/9999");
        });
    </script>


    <!--disables Submit Button when form is submitted
    <script type="text/javascript">
        $(".submitBtn").click(function () {
            $('#form1').submit(); //submit form
            $(".submitBtn").attr("disabled", "disabled");
        });
    </script>-->



</asp:Content>
<asp:Content ContentPlaceHolderID="hero" runat="server">
    <section class="full_background clouds_mountain scenic_background">
	    <div class="bl_trans_bg">
            <div class="container">
                <div class="row">
                    <div class="column">
                        <h4>Blue Sky RV Insurance Quote</h4>
                        <p>The Blue Sky RV Insurance program is currently not available for vehicles registered in the following states: AL, AK, CT, DE, FL, HI, KY, MA, MD, ME, MS, NH, NY, RI, VT and WY. If your registration state is listed above please check back soon as we are continuing to expand and add new states to serve you. Coverage is also not available on stationary trailers or park model travel trailers that are registered or garaged in FL, GA, LA, NC, NJ, SC, TX, and VA. </p>
                    </div>
                </div>
            </div>
	    </div>
    </section>
</asp:Content>
<asp:Content ContentPlaceHolderID="main" runat="server">

	<asp:Panel ID="pnlErrors" runat="server" Visible="false">
		<script type="text/javascript">
		    document.getElementById('bodyOveride').style.overflow = 'hidden';
		    document.getElementById('htmlOveride').style.overflow = 'hidden';
		</script>
		<table class="floatingTable u-full-width" border="0">
			<tr>
				<td>
					<div class="floatingPanel">
                        <asp:Button ID="cmdFloatClose" runat="server" Text="X" CssClass="common_button" OnClick="cmdFloatClose_Close" OnClientClick="javascript:alertStatus=false" />
						<asp:Panel ID="pnlDefault" runat="server" Visible="true">
							<div class="floatingPanelHeader">
							</div>
							<div class="floatingPanelContentStop">
							</div>
							<div class="floatingPanelContent">
								<div class="floatingPanelContentText">
									<asp:Literal ID="litErrors" runat="server" />
								</div>
							</div>
						</asp:Panel>
						<asp:Panel ID="pnlAlt" runat="server" Visible="false">
							<div class="floatingPanelHeaderAlt">
							</div>
							<div class="floatingPanelContent">
								<div class="floatingPanelContentTextAlt">
									<asp:Literal ID="litNoErrors" runat="server" />
								</div>
							</div>
						</asp:Panel>
					</div>
				</td>
			</tr>
		</table>
		<div class="floatingCore">
		</div>
	</asp:Panel>

	<asp:Panel ID="pnlPanel3" runat="server" Visible="true">
        <h4>RV Insurance Quote: Vehicles</h4>
        <p class="header_text_disclaimer"><strong>Important Note:</strong> Only motorized units with a value between $20,000 and $2,000,000, and towable units with a 
value between $5,000 and $250,000 are eligible for our program. All vehicles must be titled AND registered to 
the person making this application for coverage.</p>
        <h5>VEHICLE #1</h5>
        <div class="row">
            <div class="twelve columns">
                <label>Please select the Vehicle Type</label>
                <asp:DropDownList ID="ddlVehicle1Type" runat="server" CssClass="u-full-width rfq_text_field vehicleType" >
					<asp:ListItem Text="" />
					<asp:ListItem Text="Conventional Motorhome (Class A)" class="1" />
					<asp:ListItem Text="Mini-Motorhome (Class C)" class="1" />
					<asp:ListItem Text="Camper Van (Class B)" class="1" />
					<asp:ListItem Text="Conventional Travel Trailer" class="2" />
					<asp:ListItem Text="Fifth Wheel Camper" class="2" />
					<asp:ListItem Text="Pop Up Camper" class="2" />
					<asp:ListItem Text="Truck Camper" class="2" />
					<asp:ListItem Text="Bus Conversion" class="1" />
					<asp:ListItem Text="Medium Duty Tow Vehicle" class="1" />
					<asp:ListItem Text="Totorhome" class="1" />
					<asp:ListItem Text="Park Model Travel Trailer" class="2" />
					<asp:ListItem Text="Utility Trailer" class="2" />
				</asp:DropDownList>
            </div>
        </div>
        <div class="row">
            <div class="two columns">
                <label>Year</label>
                <asp:DropDownList ID="ddlVehicle1Year" runat="server" CssClass="u-full-width rfq_text_field">
					<asp:ListItem Text="" />
					<asp:ListItem Text="2016" />
					<asp:ListItem Text="2015" />
					<asp:ListItem Text="2014" />
					<asp:ListItem Text="2013" />
					<asp:ListItem Text="2012" />
					<asp:ListItem Text="2011" />
					<asp:ListItem Text="2010" />
					<asp:ListItem Text="2009" />
					<asp:ListItem Text="2008" />
					<asp:ListItem Text="2007" />
					<asp:ListItem Text="2006" />
					<asp:ListItem Text="2005" />
					<asp:ListItem Text="2004" />
					<asp:ListItem Text="2003" />
					<asp:ListItem Text="2002" />
					<asp:ListItem Text="2001" />
					<asp:ListItem Text="2000" />
					<asp:ListItem Text="1999" />
					<asp:ListItem Text="1998" />
					<asp:ListItem Text="1997" />
					<asp:ListItem Text="1996" />
					<asp:ListItem Text="1995" />
					<asp:ListItem Text="1994" />
					<asp:ListItem Text="1993" />
					<asp:ListItem Text="1992" />
					<asp:ListItem Text="1991" />
					<asp:ListItem Text="1990" />
					<asp:ListItem Text="1989" />
					<asp:ListItem Text="1988" />
					<asp:ListItem Text="1987" />
					<asp:ListItem Text="1986" />
				</asp:DropDownList>
            </div>
            <div class="five columns">
                <label>Make</label>
                <asp:TextBox ID="txtVehicle1Make" runat="server" MaxLength="15" CssClass="u-full-width rfq_text_field" />
            </div>
            <div class="five columns">
                <label>Model</label>
                <asp:TextBox ID="txtVehicle1Model" runat="server" MaxLength="15" CssClass="u-full-width rfq_text_field" />
            </div>
        </div>
        <div class="row">
            <div class="six columns">
                <label>Original Purchase Price</label>
                <asp:TextBox ID="txtVehicle1MSRP" runat="server" MaxLength="7" CssClass="u-full-width rfq_text_field price" />
            </div>
            <div class="six columns">
                <label>Current Value of the RV</label>
                <asp:TextBox ID="txtVehicle1BlueBook" runat="server" MaxLength="7" CssClass="u-full-width rfq_text_field price" />
            </div>
        </div>
        <div class="row">
            <div class="six columns">
                <label>Are you the original owner of the vehicle</label>
                <asp:DropDownList ID="ddlVehicle1OriginalOwner" runat="server" CssClass="u-full-width rfq_text_field">
					<asp:ListItem Text="" />
					<asp:ListItem Text="Yes" />
					<asp:ListItem Text="No" />
				</asp:DropDownList>
            </div>
             <div class="six columns">
                 <label>Approximate purchase date of the vehicle  (mm/dd/yyyy)</label>
                 <asp:TextBox ID="txtVehicle1PurchaseDate" runat="server" MaxLength="10" CssClass="u-full-width rfq_text_field date" />
            </div>
        </div>
        <div class="row">
            <div class="four columns style6">
                <label class="u-pull-left">Registration State</label>&nbsp;(<asp:LinkButton ID="lnkWhatsThisState" runat="server" OnClick="lnkWhatsThisState_Click" OnClientClick="javascript:alertStatus=false">What's This?</asp:LinkButton>)
				<asp:DropDownList ID="DropDownList2" runat="server" CssClass="u-full-width rfq_text_field" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged" onchange="alertStatus=false;"
					AutoPostBack="True" onclientitemselected="javascript:alertStatus=false;">
					<asp:ListItem Text="Select" />
					<asp:ListItem Text="Arizona" Value="AZ" />
					<asp:ListItem Text="Arkansas" Value="AR" />
					<asp:ListItem Text="California" Value="CA" />
					<asp:ListItem Text="Colorado" Value="CO" />
				<%--	<asp:ListItem Text="Florida" Value="FL" />--%>
					<asp:ListItem Text="Georgia" Value="GA" />
					<asp:ListItem Text="Idaho" Value="ID" />
					<asp:ListItem Text="Illinois" Value="IL" />
					<asp:ListItem Text="Indinana" Value="IN" />
					<asp:ListItem Text="Iowa" Value="IA" />
					<asp:ListItem Text="Kansas" Value="KS" />
					<asp:ListItem Text="Louisana" Value="LA" />
					<asp:ListItem Text="Michigan" Value="MI" />
					<asp:ListItem Text="Minnesota" Value="MN" />
					<asp:ListItem Text="Missouri" Value="MO" />
					<asp:ListItem Text="Montana" Value="MT" />
					<asp:ListItem Text="Nebraska" Value="NE" />
					<asp:ListItem Text="New Hampshire" Value="NH" />
					<asp:ListItem Text="New Jersey" Value="NJ" />
					<asp:ListItem Text="New Mexico" Value="NM" />
					<asp:ListItem Text="North Carolina" Value="NC" />
					<asp:ListItem Text="North Dakota" Value="ND" />
					<asp:ListItem Text="Ohio" Value="OH" />
					<asp:ListItem Text="Oklahoma" Value="OK" />
					<asp:ListItem Text="Oregon" Value="OR" />
					<asp:ListItem Text="Pennsylvania" Value="PA" />
					<asp:ListItem Text="South Carolina" Value="SC" />
					<asp:ListItem Text="South Dakota" Value="SD" />
					<asp:ListItem Text="Tennessee" Value="TN" />
					<asp:ListItem Text="Texas" Value="TX" />
					<asp:ListItem Text="Utah" Value="UT" />
					<asp:ListItem Text="Virginia" Value="VA" />
					<asp:ListItem Text="Washington" Value="WA" />
					<asp:ListItem Text="Wisconsin" Value="WI" />
				</asp:DropDownList>					
                <asp:Label ID="Labelnc1" runat="server" Text="The gross vehicle weight of all motorized vehicles must exceed 26,000 pounds in North Carolina." Visible="False"></asp:Label>
            </div>
             <div class="four columns" id="zip_codeR">
                 <label>Zip Code where unit was registered</label>
                 <asp:TextBox ID="txtVehicle1ZipRegister" runat="server" MaxLength="10" CssClass="u-full-width rfq_text_field" />
            </div>
             <div class="four columns" id="zip_code6">
                 <label>Zip Code where unit is garaged </label>
                 <asp:TextBox ID="txtVehicle1ZipGarage" runat="server" MaxLength="10" CssClass="u-full-width rfq_text_field" />
            </div>
        </div>
        <div class="row">
            <div class="six columns" id="TableRowVeh1Q1" runat="server">
                <label>Does the unit have an Anti-Theft device?&nbsp;</label>
                <asp:DropDownList ID="ddlVehicle1AntiTheft" runat="server" CssClass="u-full-width rfq_text_field">
					<asp:ListItem Value="No" Text="No" Selected="True" />
					<asp:ListItem Value="Yes" Text="Yes" />
				</asp:DropDownList>
            </div>
                <div class="six columns" id="TableRowVeh1Q2" runat="server">
                    <label>Does the unit have Air bags?</label>
                    <asp:DropDownList ID="ddlVehicle1AirBags" runat="server" CssClass="u-full-width rfq_text_field">
					<asp:ListItem Value="No" Text="No" Selected="True" />
					<asp:ListItem Value="Yes" Text="Yes" />
				</asp:DropDownList>
            </div>
        </div>
        <div class="row">
            <div class="six columns" id="TableRowVeh1Q3" runat="server">
                <label>Does the unit have an Anti-Lock Brakes?</label>
                <asp:DropDownList ID="ddlVehicle1ABS" runat="server" CssClass="u-full-width rfq_text_field">
					<asp:ListItem Value="No" Text="No" Selected="True" />
					<asp:ListItem Value="Yes" Text="Yes" />
				</asp:DropDownList>
            </div>
             <div class="six columns" id="TableRowVeh1Q4" runat="server">
                 <label>Is the RV garaged when not in use?</label>
                 <asp:DropDownList ID="ddlVehicle1Garaged" runat="server" CssClass="u-full-width rfq_text_field">
					<asp:ListItem Value="No" Text="No" Selected="True" />
					<asp:ListItem Value="Yes" Text="Yes" />
				</asp:DropDownList>
            </div>
        </div>
        <div class="row">
            <div class="six columns" id="TableRowVehicle1Deductible" runat="server">
                <label>Preferred deductible to be quoted</label>
                <asp:DropDownList ID="ddlVehicle1PreferredDeductable" runat="server" CssClass="u-full-width rfq_text_field">
					<asp:ListItem Value="250" Selected="False">$250</asp:ListItem>
					<asp:ListItem Value="500" Selected="True">$500</asp:ListItem>
					<asp:ListItem Value="1000" Selected="False">$1,000</asp:ListItem>
					<asp:ListItem Value="2500" Selected="False">$2,500</asp:ListItem>
					<asp:ListItem Value="5000" Selected="False">$5,000</asp:ListItem>
				</asp:DropDownList>
            </div>
             <div class="six columns" id="TableRowVeh1Q5" runat="server">
                 <label class="u-pull-left">Is the unit stationary the entire year?</label> &nbsp;(<asp:LinkButton ID="lnkWhatsThisStationary1" runat="server" OnClick="lnkWhatsThisStationary1_Click" OnClientClick="javascript:alertStatus=false">What's This?</asp:LinkButton>)
                 <asp:DropDownList ID="DropDownListStationary1" runat="server" CssClass="u-full-width rfq_text_field">
					<asp:ListItem Value="No" Text="No" Selected="True" />
					<asp:ListItem Value="Yes" Text="Yes" />
				</asp:DropDownList>
            </div>
        </div>
        <div class="row">
            <div class="six columns" id="TableRowVeh1Q6" runat="server">
                <label>Is the unit made of fiberglass construction?</label>
                <asp:DropDownList ID="DropDownListFiberglass1" runat="server" CssClass="u-full-width rfq_text_field">
					<asp:ListItem Value="No" Text="No" Selected="True" />
					<asp:ListItem Value="Yes" Text="Yes" />
				</asp:DropDownList>
            </div>
             <div class="six columns" id="TableRowVeh1Q7" runat="server">
                 <label>Does the unit have a rubberized hail-proof roof?</label>
                 <asp:DropDownList ID="DropDownListHailProof1" runat="server" CssClass="u-full-width rfq_text_field">
					<asp:ListItem Value="No" Text="No" Selected="True" />
					<asp:ListItem Value="Yes" Text="Yes" />
				</asp:DropDownList>
            </div>
        </div>
        <div class="row">
            <div class="six columns" id="TableRowVeh1Q8" runat="server">
                <label>Do you rent this vehicle to others?</label>
                <asp:DropDownList ID="DropDownListRental1" runat="server" CssClass="u-full-width rfq_text_field">
					<asp:ListItem Value="No" Text="No" Selected="True" />
					<asp:ListItem Value="Yes" Text="Yes" />
				</asp:DropDownList>
            </div>
             <div class="six columns">

            </div>
        </div>

         <hr />
        <h5>VEHICLE #2</h5>


        <div class="row">
            <div class="twelve columns">
                <label>Please select the Vehicle Type</label>
                <asp:DropDownList ID="ddlVehicle2Type" runat="server" CssClass="u-full-width rfq_text_field vehicleType2" >
					<asp:ListItem Text="" />
					<asp:ListItem Text="Conventional Motorhome (Class A)" class="1" />
					<asp:ListItem Text="Mini-Motorhome (Class C)" class="1" />
					<asp:ListItem Text="Camper Van (Class B)" class="1" />
					<asp:ListItem Text="Conventional Travel Trailer" class="2" />
					<asp:ListItem Text="Fifth Wheel Camper" class="2" />
					<asp:ListItem Text="Pop Up Camper" class="2" />
					<asp:ListItem Text="Truck Camper" class="2" />
					<asp:ListItem Text="Bus Conversion" class="1" />
					<asp:ListItem Text="Medium Duty Tow Vehicle" class="1" />
					<asp:ListItem Text="Totorhome" class="1" />
					<asp:ListItem Text="Park Model Travel Trailer" class="2" />
					<asp:ListItem Text="Utility Trailer" class="2" />
				</asp:DropDownList>
            </div>
        </div>
        <div class="row">
            <div class="two columns">
                <label>Year</label>
                <asp:DropDownList ID="ddlVehicle2Year" runat="server" CssClass="u-full-width rfq_text_field">
					<asp:ListItem Text="" />
					<asp:ListItem Text="2016" />
					<asp:ListItem Text="2015" />
					<asp:ListItem Text="2014" />
					<asp:ListItem Text="2013" />
					<asp:ListItem Text="2012" />
					<asp:ListItem Text="2011" />
					<asp:ListItem Text="2010" />
					<asp:ListItem Text="2009" />
					<asp:ListItem Text="2008" />
					<asp:ListItem Text="2007" />
					<asp:ListItem Text="2006" />
					<asp:ListItem Text="2005" />
					<asp:ListItem Text="2004" />
					<asp:ListItem Text="2003" />
					<asp:ListItem Text="2002" />
					<asp:ListItem Text="2001" />
					<asp:ListItem Text="2000" />
					<asp:ListItem Text="1999" />
					<asp:ListItem Text="1998" />
					<asp:ListItem Text="1997" />
					<asp:ListItem Text="1996" />
					<asp:ListItem Text="1995" />
					<asp:ListItem Text="1994" />
					<asp:ListItem Text="1993" />
					<asp:ListItem Text="1992" />
					<asp:ListItem Text="1991" />
					<asp:ListItem Text="1990" />
					<asp:ListItem Text="1989" />
					<asp:ListItem Text="1988" />
					<asp:ListItem Text="1987" />
					<asp:ListItem Text="1986" />
				</asp:DropDownList>
            </div>
            <div class="five columns">
                <label>Make</label>
                <asp:TextBox ID="txtVehicle2Make" runat="server" MaxLength="15" CssClass="u-full-width rfq_text_field" />
            </div>
            <div class="five columns">
                <label>Model</label>
                <asp:TextBox ID="txtVehicle2Model" runat="server" MaxLength="15" CssClass="u-full-width rfq_text_field" />
            </div>
        </div>
        <div class="row">
            <div class="six columns">
                <label>Original Purchase Price</label>
                <asp:TextBox ID="txtVehicle2MSRP" runat="server" MaxLength="7" CssClass="u-full-width rfq_text_field price" />
            </div>
            <div class="six columns">
                <label>Current Value of the RV</label>
                <asp:TextBox ID="txtVehicle2BlueBook" runat="server" MaxLength="7" CssClass="u-full-width rfq_text_field price" />
            </div>
        </div>
         <div class="row">
            <div class="six columns">
                <label>Are you the original owner of the vehicle</label>
                <asp:DropDownList ID="ddlVehicle2OriginalOwner" runat="server" CssClass="u-full-width rfq_text_field">
					<asp:ListItem Text="" />
					<asp:ListItem Text="Yes" />
					<asp:ListItem Text="No" />
				</asp:DropDownList>
            </div>
             <div class="six columns">
                 <label>Approximate purchase date of the vehicle  (mm/dd/yyyy)</label>
                 <asp:TextBox ID="txtVehicle2PurchaseDate" runat="server" MaxLength="10" CssClass="u-full-width rfq_text_field date" />
            </div>
        </div>
         <div class="row">
            <div class="four columns style6">
                <label class="u-pull-left">Registration State</label>
                <asp:DropDownList ID="DropDownList1" runat="server"
                    CssClass="u-full-width rfq_text_field"
                    OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" onchange="alertStatus=false;"
                    AutoPostBack="True" onclientitemselected="javascript:alertStatus=false;">
                    <asp:ListItem Text="Select" />
                    <asp:ListItem Text="Arizona" Value="AZ" />
                    <asp:ListItem Text="Arkansas" Value="AR" />
                    <asp:ListItem Text="California" Value="CA" />
                    <asp:ListItem Text="Colorado" Value="CO" />
                    <asp:ListItem Text="Florida" Value="FL" />
                    <asp:ListItem Text="Georgia" Value="GA" />
                    <asp:ListItem Text="Idaho" Value="ID" />
                    <asp:ListItem Text="Illinois" Value="IL" />
                    <asp:ListItem Text="Indinana" Value="IN" />
                    <asp:ListItem Text="Iowa" Value="IA" />
                    <asp:ListItem Text="Kansas" Value="KS" />
                    <asp:ListItem Text="Louisana" Value="LA" />
                    <asp:ListItem Text="Michigan" Value="MI" />
                    <asp:ListItem Text="Minnesota" Value="MN" />
                    <asp:ListItem Text="Missouri" Value="MO" />
                    <asp:ListItem Text="Montana" Value="MT" />
                    <asp:ListItem Text="Nebraska" Value="NE" />
                    <asp:ListItem Text="New Hampshire" Value="NH" />
                    <asp:ListItem Text="New Jersey" Value="NJ" />
                    <asp:ListItem Text="New Mexico" Value="NM" />
                    <asp:ListItem Text="North Carolina" Value="NC" />
                    <asp:ListItem Text="North Dakota" Value="ND" />
                    <asp:ListItem Text="Ohio" Value="OH" />
                    <asp:ListItem Text="Oklahoma" Value="OK" />
                    <asp:ListItem Text="Oregon" Value="OR" />
                    <asp:ListItem Text="Pennsylvania" Value="PA" />
                    <asp:ListItem Text="South Carolina" Value="SC" />
                    <asp:ListItem Text="South Dakota" Value="SD" />
                    <asp:ListItem Text="Tennessee" Value="TN" />
                    <asp:ListItem Text="Texas" Value="TX" />
                    <asp:ListItem Text="Utah" Value="UT" />
                    <asp:ListItem Text="Virginia" Value="VA" />
                    <asp:ListItem Text="Washington" Value="WA" />
                    <asp:ListItem Text="Wisconsin" Value="WI" />
                </asp:DropDownList>				
                <asp:Label ID="Labelnc2" runat="server" Text="The gross vehicle weight of all motorized vehicles must exceed 26,000 pounds in North Carolina." Visible="False"></asp:Label>
            </div>
             <div class="four columns" id="zip_codeR">
                 <label>Zip Code where unit was registered</label>
                 <asp:TextBox ID="txtVehicle2ZipRegister" runat="server" MaxLength="10" CssClass="u-full-width rfq_text_field" />
            </div>
             <div class="four columns" id="zip_code6">
                 <label>Zip Code where unit is garaged </label>
                 <asp:TextBox ID="txtVehicle2ZipGarage" runat="server" MaxLength="10" CssClass="u-full-width rfq_text_field" />
        </div>
         <div class="row">
            <div class="six columns" id="TableRowVeh2Q1" runat="server">
                <label>Does the unit have an Anti-Theft device?&nbsp;</label>
                <asp:DropDownList ID="ddlVehicle2AntiTheft" runat="server" CssClass="u-full-width rfq_text_field">
					<asp:ListItem Value="No" Text="No" Selected="True" />
					<asp:ListItem Value="Yes" Text="Yes" />
				</asp:DropDownList>
            </div>
             <div class="six columns" id="TableRowVeh2Q2" runat="server">
                 <label>Does the unit have Air bags?</label>
                 <asp:DropDownList ID="ddlVehicle2AirBags" runat="server" CssClass="u-full-width rfq_text_field">
					<asp:ListItem Value="No" Text="No" Selected="True" />
					<asp:ListItem Value="Yes" Text="Yes" />
				</asp:DropDownList>
            </div>
        </div>
         <div class="row">
            <div class="six columns" id="TableRowVeh2Q3" runat="server">
                <label>Does the unit have an Anti-Lock Brakes?</label>
                <asp:DropDownList ID="ddlVehicle2ABS" runat="server" CssClass="u-full-width rfq_text_field">
					<asp:ListItem Value="No" Text="No" Selected="True" />
					<asp:ListItem Value="Yes" Text="Yes" />
				</asp:DropDownList>
            </div>
             <div class="six columns" id="TableRowVeh2Q4" runat="server">
                 <label>Is the RV garaged when not in use?</label>
                 <asp:DropDownList ID="ddlVehicle2Garaged" runat="server" CssClass="u-full-width rfq_text_field">
					<asp:ListItem Value="No" Text="No" Selected="True" />
					<asp:ListItem Value="Yes" Text="Yes" />
				</asp:DropDownList>
            </div>
        </div>
        <div class="row">
            <div class="six columns" id="TableRowVehicle2Deductible" runat="server">
                <label>Preferred deductible to be quoted</label>
                <asp:DropDownList ID="ddlVehicle2PreferredDeductable" runat="server" CssClass="u-full-width rfq_text_field">
					<asp:ListItem Value="250" Selected="False">$250</asp:ListItem>
					<asp:ListItem Value="500" Selected="True">$500</asp:ListItem>
					<asp:ListItem Value="1000" Selected="False">$1,000</asp:ListItem>
					<asp:ListItem Value="2500" Selected="False">$2,500</asp:ListItem>
					<asp:ListItem Value="5000" Selected="False">$5,000</asp:ListItem>
				</asp:DropDownList>
            </div>
             <div class="six columns" id="TableRowVeh2Q5" runat="server">
                 <label class="u-pull-left">Is the unit stationary the entire year?</label> 
                 <asp:DropDownList ID="DropDownList3" runat="server" CssClass="u-full-width rfq_text_field">
					<asp:ListItem Value="No" Text="No" Selected="True" />
					<asp:ListItem Value="Yes" Text="Yes" />
				</asp:DropDownList>
            </div>
        </div>
        <div class="row">
            <div class="six columns" id="TableRowVeh2Q6" runat="server">
                <label>Is the unit made of fiberglass construction?</label>
                <asp:DropDownList ID="DropDownListFiberglass2" runat="server" CssClass="u-full-width rfq_text_field">
					<asp:ListItem Value="No" Text="No" Selected="True" />
					<asp:ListItem Value="Yes" Text="Yes" />
				</asp:DropDownList>
            </div>
             <div class="six columns" id="TableRowVeh2Q7" runat="server">
                 <label>Does the unit have a rubberized hail-proof roof?</label>
                 <asp:DropDownList ID="DropDownListHailProof2" runat="server" CssClass="u-full-width rfq_text_field">
					<asp:ListItem Value="No" Text="No" Selected="True" />
					<asp:ListItem Value="Yes" Text="Yes" />
				</asp:DropDownList>
            </div>
        </div>
        <div class="row">
            <div class="six columns" id="TableRowVeh2Q8" runat="server">
                <label>Do you rent this vehicle to others?</label>
                <asp:DropDownList ID="DropDownListRental2" runat="server" CssClass="u-full-width rfq_text_field">
					<asp:ListItem Value="No" Text="No" Selected="True" />
					<asp:ListItem Value="Yes" Text="Yes" />
				</asp:DropDownList>
            </div>
             <div class="six columns">

            </div>
        </div>
						
						<!-- end vehicle -->
					</asp:Panel>
					<br/>
					<asp:Panel ID="pnlControls" runat="server" Visible="true">
							<asp:Button ID="cmdBack" runat="server" Text="Back" CssClass="button" OnClick="cmdBack_Click" OnClientClick="javascript:alertStatus=false" />
							&nbsp;
							<asp:Button ID="cmdNext" runat="server" Text="Submit" CssClass="button-primary submitBtn" OnClick="cmdNext_Click" OnClientClick="javascript:alertStatus=false" />
					</asp:Panel>
    <p class="header_text_disclaimer">In connection with this application for insurance we will collect information from consumer reporting agencies such as driving records. We may also obtain an insurance credit score. We may use a third party in connection with the development of your insurance credit score. By submitting this request for quote you are providing permission for us to obtain driving record and an insurance credit score.</p>

</asp:Content>