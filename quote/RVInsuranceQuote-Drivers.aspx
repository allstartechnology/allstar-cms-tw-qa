<%@ page language="C#" masterpagefile="~/AppQuote.master" autoeventwireup="true" inherits="rfqd, App_Web_kzgrcpjv" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
    <title>RV Insurance Quote | Driver | Blue Sky RV Insurance</title>
	<meta http-equiv="Keywords" name="Keywords" content="RV Insurance Quote" />
	<meta http-equiv="Description" name="Description" content="Request an RV Insurance Quote from Blue Sky RV Insurance." />
	<script type="text/javascript">
		$(document).ready(function () { $('#Tab6').addClass("selected"); });
		window.onbeforeunload = confirmExit;
		alertStatus = true;
		function confirmExit() {
			if (alertStatus == true) return "All the information that you've entered will be lost.";
		}
	</script>
    <script type="text/javascript" src="<%= Application["AppPath"] %>/jquery.maskedinput.js"></script>
    <script type="text/javascript">
        jQuery(function ($) {
            $(".date").mask("99/99/9999");
            $(".phone").mask("999-999-9999");
        });
    </script>
	<style type="text/css">
		.text { line-height: 29px; }
		div.rfq_content { height: 900px; }
	</style>
    <script type="text/javascript">
        $(document).ready(function () {
            //called when key is pressed in textbox
            $(".numerical").keypress(function (e) {
                //if the letter is not digit then display error and don't type anything
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            });
        });
	</script>  
</asp:Content>
<asp:Content ContentPlaceHolderID="hero" runat="server">
    <section class="full_background clouds_mountain scenic_background">
	    <div class="bl_trans_bg">
            <div class="container">
                <div class="row">
                    <div class="column">
                        <h4>Blue Sky RV Insurance Quote</h4>
                        <p>The Blue Sky RV Insurance program is currently not available for vehicles registered in the following states: AL, AK, CT, DE, FL, HI, KY, MA, MD, ME, MS, NH, NY, RI, VT and WY. If your registration state is listed above please check back soon as we are continuing to expand and add new states to serve you. Coverage is also not available on stationary trailers or park model travel trailers that are registered or garaged in GA, LA, NC, NJ, SC, TX, and VA. </p>
                    </div>
                </div>
            </div>
	    </div>
    </section>
</asp:Content>
<asp:Content ContentPlaceHolderID="main" runat="server">
    <asp:Panel ID="pnlErrors" runat="server" Visible="false">
		<script type="text/javascript">
			document.getElementById('bodyOveride').style.overflow = 'hidden';
			document.getElementById('htmlOveride').style.overflow = 'hidden';
		</script>
		<table class="floatingTable u-full-width" border="0">
			<tr>
				<td>
					<div class="floatingPanel">
                        <asp:Button ID="cmdFloatClose" runat="server" Text="Close" CssClass="common_button" OnClick="cmdFloatClose_Close" OnClientClick="javascript:alertStatus=false" />
						<asp:Panel ID="pnlDefault" runat="server" Visible="true">
							<div class="floatingPanelHeader">
							</div>
							<div class="floatingPanelContentStop">
							</div>
							<div class="floatingPanelContent">
								<div class="floatingPanelContentText">
									<asp:Literal ID="litErrors" runat="server" />
								</div>
							</div>
						</asp:Panel>
						<asp:Panel ID="pnlAlt" runat="server" Visible="false">
							<div class="floatingPanelHeaderAlt">
							</div>
							<div class="floatingPanelContent">
								<div class="floatingPanelContentTextAlt">
									<asp:Literal ID="litNoErrors" runat="server" />
								</div>
							</div>
						</asp:Panel>
					</div>
				</td>
			</tr>
		</table>
		<div class="floatingCore">
		</div>
	</asp:Panel>
    <asp:Panel ID="pnlPanel2" runat="server" Visible="true">
        <h4>RV Insurance Quote: Drivers</h4>
        <h5>DRIVER #1</h5>
        <div class="row">
            <div class="three columns">
                <label>Birth Date</label>
                <asp:TextBox ID="txtDriver1DateOfBirth" placeholder="XX/XX/XXXX" runat="server" MaxLength="10" CssClass="u-full-width rfq_text_field date birthdatepicker" />
            </div>
            <div class="three columns">
                <label>Marital Status</label>
                <asp:DropDownList ID="ddlDriver1MartialStatus" runat="server" CssClass="u-full-width rfq_text_field">
					<asp:ListItem Text="Married" />
					<asp:ListItem Text="Single" />
					<asp:ListItem Text="Widowed" />
				</asp:DropDownList>
            </div>
            <div class="six columns">
                <label>Does the driver have a valid U.S. Driver&#8217;s License?&nbsp;</label>
                <asp:DropDownList ID="ddlDriver1ValidUSLicense" runat="server" CssClass="u-full-width rfq_text_field">
					<asp:ListItem Text="Yes" />
					<asp:ListItem Text="No" />
				</asp:DropDownList>
            </div>
        </div>
        <div class="row">
            <div class="six columns">
                <label class="u-pull-left">Number of Major Violations in the past 36 months&nbsp;</label>&nbsp;(<asp:LinkButton ID="LinkButton1" runat="server" OnClick="lnkWhatsThisMajor_Click" OnClientClick="javascript:alertStatus=false">What's This?</asp:LinkButton>)
                <asp:DropDownList ID="ddlDriver1MajorViolations" runat="server" CssClass="u-full-width rfq_text_field">
					<asp:ListItem Text="0" />
					<asp:ListItem Text="1" />
					<asp:ListItem Text="2" />
				</asp:DropDownList>
            </div>
            <div class="six columns">
                <label class="u-pull-left">Number of Minor Violations in the past 36 months&nbsp;</label>&nbsp;(<asp:LinkButton ID="LinkButton2" runat="server" OnClick="lnkWhatsThisMinor_Click" OnClientClick="javascript:alertStatus=false">What's This?</asp:LinkButton>)
				<asp:DropDownList ID="ddlDriver1MinorViolations" runat="server" CssClass="u-full-width rfq_text_field">
					<asp:ListItem Text="0" />
					<asp:ListItem Text="1" />
					<asp:ListItem Text="2" />
					<asp:ListItem Text="3" />
					<asp:ListItem Text="4" />
					<asp:ListItem Text="5" />
					<asp:ListItem Text="6" />
				</asp:DropDownList>
            </div>
        </div>
        <div class="row">
            <div class="six columns">
				<label>Number of At Fault Accidents in the past 36 months</label>
                <asp:DropDownList ID="ddlDriver1AtFault" runat="server" CssClass="u-full-width rfq_text_field">
					<asp:ListItem Text="0" />
					<asp:ListItem Text="1" />
					<asp:ListItem Text="2" />
				</asp:DropDownList>
			</div>
            <div class="six columns">
				<label>Years of RV Operating Experience</label>
				<asp:TextBox ID="TextBoxYearsExperience1" runat="server" MaxLength="10" Width="150px" CssClass="u-full-width rfq_text_field numerical" />
			</div>
        </div>
        <div class="row">
            <div class="six columns">
				<label>Has the driver completed an RV Safety Driving Course in the past 36 months?</label>
                <asp:DropDownList ID="ddlDriver1RVSafetyCourse" runat="server" CssClass="u-full-width rfq_text_field">
					<asp:ListItem Text="No" Selected="True" />
					<asp:ListItem Text="Yes" />
				</asp:DropDownList>
			</div>
			<div class="six columns">
				<label>Does the driver possess a valid Commercial Drivers License (CDL)?</label>
                <asp:DropDownList ID="ddlDriver1CDL" runat="server" CssClass="u-full-width rfq_text_field">
					<asp:ListItem Text="No" Selected="True" />
					<asp:ListItem Text="Yes" />
				</asp:DropDownList>
			</div>
        </div>

        <hr />
        <h5>DRIVER #2</h5>
        <div class="row">
            <div class="three columns">
				<label>Birth Date</label>
				<asp:TextBox ID="txtDriver2DateOfBirth" runat="server" MaxLength="10" Width="150px" CssClass="u-full-width rfq_text_field date birthdatepicker" />
			</div>
			<div class="three columns">
				<label>Status</label>
				<asp:DropDownList ID="ddlDriver2MartialStatus" runat="server" CssClass="u-full-width rfq_text_field">
					<asp:ListItem Text="Married" />
					<asp:ListItem Text="Single" />
					<asp:ListItem Text="Widowed" />
				</asp:DropDownList>
			</div>
			<div class="six columns">
				<label>Does the driver have a valid U.S. Driver&#8217;s License?</label>
				<asp:DropDownList ID="ddlDriver2ValidUSLicense" runat="server" CssClass="u-full-width rfq_text_field">
					<asp:ListItem Text="Yes" />
					<asp:ListItem Text="No" />
				</asp:DropDownList>
			</div>
        </div>
        <div class="row">
            <div class="six columns">
				<label class="u-pull-left">Number of Major Violations in the past 36 months</label>&nbsp;(<asp:LinkButton ID="LinkButton3" runat="server" OnClick="lnkWhatsThisMajor_Click" OnClientClick="javascript:alertStatus=false">What's This?</asp:LinkButton>)
				<asp:DropDownList ID="ddlDriver2MajorViolations" runat="server" CssClass="u-full-width rfq_text_field">
					<asp:ListItem Text="0" />
					<asp:ListItem Text="1" />
					<asp:ListItem Text="2" />
				</asp:DropDownList>
				</div>
			<div class="six columns">
				<label class="u-pull-left">Number of Minor Violations in the past 36 months</label>&nbsp;(<asp:LinkButton ID="LinkButton4" runat="server" OnClick="lnkWhatsThisMinor_Click" OnClientClick="javascript:alertStatus=false">What's This?</asp:LinkButton>)
				<asp:DropDownList ID="ddlDriver2MinorViolations" runat="server" CssClass="u-full-width rfq_text_field">
					<asp:ListItem Text="0" />
					<asp:ListItem Text="1" />
					<asp:ListItem Text="2" />
					<asp:ListItem Text="3" />
					<asp:ListItem Text="4" />
					<asp:ListItem Text="5" />
					<asp:ListItem Text="6" />
				</asp:DropDownList>
				</div>
        </div>
        <div class="row">
            <div class="six columns">
			    <label>Number of At Fault Accidents in the past 36 months</label>
                <asp:DropDownList ID="ddlDriver2AtFault" runat="server" CssClass="u-full-width rfq_text_field">
					<asp:ListItem Text="0" />
					<asp:ListItem Text="1" />
					<asp:ListItem Text="2" />
				</asp:DropDownList>
			</div>
            <div class="six columns">
				<label>Years of RV Operating Experience</label>
				<asp:TextBox ID="TextBoxYearsExperience2" runat="server" MaxLength="10" Width="150px" CssClass="u-full-width rfq_text_field numerical" />
			</div>
        </div>
        <div class="row">
            <div>
			    <label>Has the driver completed an RV Safety Driving Course in the past 36 months?</label>
                <asp:DropDownList ID="ddlDriver2RVSafetyCourse" runat="server" CssClass="u-full-width rfq_text_field">
					<asp:ListItem Text="No" Selected="True" />
					<asp:ListItem Text="Yes" />
				</asp:DropDownList>
			</div>
			<div>
				   <label>Does the driver possess a valid Commercial Drivers License (CDL)?</label>
                    <asp:DropDownList ID="ddlDriver2CDL" runat="server" CssClass="u-full-width rfq_text_field">
					<asp:ListItem Text="No" Selected="True" />
					<asp:ListItem Text="Yes" />
				</asp:DropDownList>
			</div>
        </div>



    </asp:Panel>
    <asp:Panel ID="pnlControls" runat="server" Visible="true">
			<asp:Button ID="cmdBack" runat="server" Text="Back" CssClass="button" OnClick="cmdBack_Click" OnClientClick="javascript:alertStatus=false" />&nbsp;
			<asp:Button ID="cmdNext" runat="server" Text="Next" CssClass="button-primary" OnClick="cmdNext_Click" OnClientClick="javascript:alertStatus=false" />
	</asp:Panel>
    <p class="header_text_disclaimer">In connection with this application for insurance we will collect information from consumer reporting agencies such as driving records. We may also obtain an insurance credit score. We may use a third party in connection with the development of your insurance credit score. By submitting this request for quote you are providing permission for us to obtain driving record and an insurance credit score.</p>
    <script type="text/javascript" src="moment.js"></script>
	<script type="text/javascript" src="pikaday.js"></script>
    <script type="text/javascript" src="pikadayJQ.js"></script>
    <script>

        var $datepicker = $('.datepicker').pikaday({
            format: 'L',
            disableWeekends: true,
            minDate: new Date(),
            maxDate: new Date(2020 - 12 - 31),
            yearRange: [2015, 2020]
        });
        // chain a few methods for the first datepicker, jQuery style!
        //$datepicker.pikaday('show').pikaday('nextMonth');
        var $datepicker = $('.birthdatepicker').pikaday({
            format: 'L',

            yearRange: [1940, 2020]
        });
        $('.birthdatepicker').pikaday('gotoYear', 1980);
    </script>

</asp:Content>
