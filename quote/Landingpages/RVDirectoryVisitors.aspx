﻿<%@ Page Language="C#" MasterPageFile="~/App.master" %>

<asp:Content ContentPlaceHolderID="head" Runat="Server">
	<title>RV Insurance | Motorhome Insurance | Blue Sky RV Insurance</title>
	<style type="text/css">
		.text { 
			position: absolute;
			top: 130px;
			left: 560px;
			width: 395px;
			font-size: 14px;
		}
	</style>
</asp:Content>

<asp:Content ContentPlaceHolderID="main" Runat="Server">
	<div class="content">
		<div class="company_info">
			<img src="<%= Application["AppPath"] %>/images/default_bg2.jpg" alt="RV Insurance, Blue Sky RV Insurance" />
		</div>
		<div class="text">
			<h1>Welcome RV Directory Visitor</h1>
			<p>Thank you for your interest in Blue Sky RV Insurance. You are now being redirected to our <b>Online Quote Form.</b></p>
			<p>If you have javascript disabled, please <a href="<%= Application["AppPath"] %>/rv-insurance-quote-intro.aspx">click here</a> to continue to your quote.</p>
			<div style="clear:both;"></div>
		</div>
	</div>
    
	<script type="text/javascript">
	    var targetURL = "<%= Application["AppPath"] %>/rv-insurance-quote-intro.aspx";
		var countdownfrom = 10;

		var currentsecond = countdownfrom + 1;
		function countredirect() {
			if (currentsecond != 1) {
				currentsecond -= 1;
				$('span#redirect2').html(currentsecond);
			}
			else {
				window.location = targetURL;
				return;
			}
			setTimeout("countredirect()", 0);
		}

		countredirect();
	</script>
</asp:Content>

