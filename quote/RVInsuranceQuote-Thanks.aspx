<%@ page language="C#" masterpagefile="~/AppQuote.master" autoeventwireup="true" inherits="rfq_thanks2, App_Web_kzgrcpjv" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
	<title>RV Insurance Quote | Blue Sky RV Insurance</title>
	<meta http-equiv="Keywords" name="Keywords" content="RV Insurance Quote" />
	<meta http-equiv="Description" name="Description" content="Thank you for requesting an RV Insurance Quote from Blue Sky RV Insurance." />
	<script type="text/javascript">
		$(document).ready(function () { $('#Tab6').addClass("selected"); });
	</script>
    <!-- Start Quantcast Tag -->
    <script type="text/javascript">
        var _qevents = _qevents || [];

        (function () {
            var elem = document.createElement('script');
            elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
            elem.async = true;
            elem.type = "text/javascript";
            var scpt = document.getElementsByTagName('script')[0];
            scpt.parentNode.insertBefore(elem, scpt);
        })();

        _qevents.push(
        { qacct: "p-VypNfW3as3_Ed", labels: "_fp.event.Quote Confirmation,_fp.pcat.INSERT+PRODUCT+CATEGORY", orderid: "INSERT+ORDER+ID" }
        );
    </script>
    <noscript>
    <img src="//pixel.quantserve.com/pixel/p-VypNfW3as3_Ed.gif?labels=_fp.event.Quote+Confirmation,_fp.pcat.INSERT+PRODUCT+CATEGORY&orderid=INSERT+ORDER+ID" style="display: none;" border="0" height="1" width="1" alt="Quantcast"/>
    </noscript>
    <!-- End Quantcast tag -->


    <!-- Google Code for Blue Sky - RV Insurance Conversion Page -->
    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 1026447193;
        var google_conversion_language = "en";
        var google_conversion_format = "3";
        var google_conversion_color = "ffffff";
        var google_conversion_label = "FSSUCKn5sVoQ2a656QM";
        var google_remarketing_only = false;
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>



    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1026447193/?label=FSSUCKn5sVoQ2a656QM&amp;guid=ON&amp;script=0"/>
    </div>
    </noscript>


</asp:Content>

<asp:Content ContentPlaceHolderID="main" runat="server">
	<div class="content" style="min-height: 382px;">
        <asp:Panel ID="pnlErrors" runat="server" Visible="false">
		<script type="text/javascript">
		    document.getElementById('bodyOveride').style.overflow = 'hidden';
		    document.getElementById('htmlOveride').style.overflow = 'hidden';
		</script>
		<table class="floatingTable">
			<tr>
				<td>
					<div class="floatingPanel">
						<asp:Panel ID="pnlDefault" runat="server" Visible="true">
							<div class="floatingPanelHeader">
							</div>
							<div class="floatingPanelContentStop">
							</div>
							<div class="floatingPanelContent">
								<div class="floatingPanelContentText">
									<asp:Literal ID="litErrors" runat="server" />
								</div>
							</div>
						</asp:Panel>
						<asp:Panel ID="pnlAlt" runat="server" Visible="false">
							<div class="floatingPanelHeaderAlt">
							</div>
							<div class="floatingPanelContent">
								<div class="floatingPanelContentTextAlt">
									<asp:Literal ID="litNoErrors" runat="server" />
								</div>
							</div>
						</asp:Panel>
						<div class="floatingPanelBottom">
							<asp:Button ID="cmdFloatClose" runat="server" Text="Close" CssClass="common_button" OnClick="cmdFloatClose_Close" OnClientClick="javascript:alertStatus=false" />
						</div>
					</div>
				</td>
			</tr>
		</table>
		<div class="floatingCore">
		</div>
	</asp:Panel>
		<div class="stuff">
			<div class="company_info">
				<div class="rfq_container_4" >

					<%--<asp:Panel ID="pnlEmail" runat="server" Visible="false">
								Thank you for requesting an RV Insurance Quote from Blue Sky RV Insurance. One of our representatives will provide an emailed quote to
								you within one business day. Please make sure that you have <a href="mailto:sales@blueskyrvinsurance.com">sales@blueskyrvinsurance.com</a> saved in your
								email address book to ensure that our email is received. If you have any questions in the interim, you can
								contact us at (866) 484-2583.
								<br />
						</asp:Panel>
						<asp:Panel ID="pnpPhone" runat="server" Visible="false">
								Thank you for requesting an RV Insurance Quote from Blue Sky RV Insurance. One of our representatives will contact by phone at the
								number you provided within one business day. If you have any questions in the interim, you can contact us at
								(866) 484-2583.
								<br />
						</asp:Panel>
						<asp:Panel ID="pnlFax" runat="server" Visible="false">
								Thank you for requesting an RV Insurance Quote from Blue Sky RV Insurance. One of our representatives will provide a quote to you via
								fax, at the number you provided, within one business day. If you have any questions in the interim, you can
								contact us at (866) 484-2583.
								<br />
						</asp:Panel>--%>
					<asp:Panel ID="PanelDefault" runat="server">
						Thank you for requesting an RV Insurance Quote from Blue Sky RV Insurance! One of our representatives will email a quote to you within one business day. If you have any questions in the interim, please contact us at (866) 484-2583.  We look forward to earning your business.
            <br />
					</asp:Panel>
                    <asp:Panel ID="PanelContact" runat="server">
						Thank you for contacting Blue Sky RV Insurance! One of our representatives will be in touch with you shortly. If you have any questions in the interim, please contact us at (866) 484-2583.  We look forward to earning your business.<br />
					</asp:Panel>
					<br />
					<asp:Panel ID="Panel" runat="server">
						<asp:Label ID="LabelAutoSubmitSuccess" runat="server"
							Text="Thank you for requesting an auto quote with your RV quote.  We look forward to serving you."
							ForeColor="#006600" Visible="False"></asp:Label>
					</asp:Panel>
					<asp:Panel ID="PanelPersonalAuto" runat="server" Visible="false">
						<div>Blue Sky offers personal auto insurance in select states. You could save up to 20% by insuring your RV and
							<br />
							auto(s) together. Request a quote today by submitting the VINs of your auto(s).
						</div>
                        <div class="row">
                            <div class="one columns"><br /><b>Veh 1</b></div>
                            <div class="five columns"><label>Year/Make/Model</label><asp:TextBox ID="TextBoxYearMakeModel1" CssClass="u-full-width" runat="server"></asp:TextBox></div>
                            <div class="one columns"><br /><b>OR</b></div>
                            <div class="five columns"><label>VIN:</label><asp:TextBox ID="TextBoxVIN1" CssClass="u-full-width" runat="server"></asp:TextBox></div>
                        </div>
                        <div class="row">
                            <div class="one columns"><br /><b>Veh 2</b></div>
                            <div class="five columns"><label>Year/Make/Model</label><asp:TextBox ID="TextBoxYearMakeModel2" CssClass="u-full-width" runat="server"></asp:TextBox></div>
                            <div class="one columns"><br /><b>OR</b></div>
                            <div class="five columns"><label>VIN:</label><asp:TextBox ID="TextBoxVIN2" CssClass="u-full-width" runat="server"></asp:TextBox></div>
                        </div>
                        <div class="row">
                            <div class="one columns"><br /><b>Veh 3</b></div>
                            <div class="five columns"><label>Year/Make/Model</label><asp:TextBox ID="TextBoxYearMakeModel3" CssClass="u-full-width" runat="server"></asp:TextBox></div>
                            <div class="one columns"><br /><b>OR</b></div>
                            <div class="five columns"><label>VIN:</label><asp:TextBox ID="TextBoxVIN3" CssClass="u-full-width" runat="server"></asp:TextBox></div>
                        </div>
                        <div class="row">
                            <div class="one columns"><br /><b>Veh 4</b></div>
                            <div class="five columns"><label>Year/Make/Model</label><asp:TextBox ID="TextBoxYearMakeModel4" CssClass="u-full-width" runat="server"></asp:TextBox></div>
                            <div class="one columns"><br /><b>OR</b></div>
                            <div class="five columns"><label>VIN:</label><asp:TextBox ID="TextBoxVIN4" CssClass="u-full-width" runat="server"></asp:TextBox></div>
                        </div>
						<asp:Button ID="ButtonSubmit" runat="server" Text="Submit" CssClass="button-primary" OnClick="ButtonSubmit_Click" />
								
					</asp:Panel>
				</div>
			</div>
		</div>
	</div>

</asp:Content>
