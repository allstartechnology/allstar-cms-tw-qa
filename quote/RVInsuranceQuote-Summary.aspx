<%@ page language="C#" masterpagefile="~/App.master" autoeventwireup="true" inherits="rfqs, App_Web_kzgrcpjv" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
	<title>RV Insurance Quote | Blue Sky RV Insurance</title>
	<meta http-equiv="Keywords" name="Keywords" content="RV Insurance Quote" />
	<meta http-equiv="Description" name="Description" content="Request an RV Insurance Quote from Blue Sky RV Insurance." />
	<script type="text/javascript">
		$(document).ready(function () { $('#Tab6').addClass("selected"); });
		window.onbeforeunload = confirmExit;
		alertStatus = true;
		function confirmExit() {
			if (alertStatus == true) return "All the information that you've entered will be lost.";
		}
	</script>
	<style type="text/css">
		div.rfq_content { height: 1830px; }
	</style>
</asp:Content>

<asp:Content ContentPlaceHolderID="main" runat="server">
	<asp:Panel ID="pnlErrors" runat="server" Visible="false">
		<script type="text/javascript">
			document.getElementById('bodyOveride').style.overflow = 'hidden';
			document.getElementById('htmlOveride').style.overflow = 'hidden';
		</script>
		<table class="floatingTable">
			<tr>
				<td>
					<div class="floatingPanel">
						<asp:Panel ID="pnlDefault" runat="server" Visible="true">
							<div class="floatingPanelHeader">
							</div>
							<div class="floatingPanelContentStop">
							</div>
							<div class="floatingPanelContent">
								<div class="floatingPanelContentText">
									<asp:Literal ID="litErrors" runat="server" />
								</div>
							</div>
						</asp:Panel>
						<asp:Panel ID="pnlAlt" runat="server" Visible="false">
							<div class="floatingPanelHeaderAlt">
							</div>
							<div class="floatingPanelContent">
								<div class="floatingPanelContentTextAlt">
									<asp:Literal ID="litNoErrors" runat="server" />
								</div>
							</div>
						</asp:Panel>
						<div class="floatingPanelBottom">
							<asp:Button ID="cmdFloatClose" runat="server" Text="Close" CssClass="common_button" OnClick="cmdFloatClose_Close" OnClientClick="javascript:alertStatus=false" />
						</div>
					</div>
				</td>
			</tr>
		</table>
		<div class="floatingCore">
		</div>
	</asp:Panel>

	<div class="rfq_content">
		<div class="stuff">
			<div class="company_info">
				<img src="images/rfq_bg.jpg" alt="RV Insurance Quote" />
				<div class="rfq_container_2 text">
					<h1>RV Insurance Quote Summary</h1>
					<asp:Panel ID="pnlPanel4" runat="server" Visible="true">
						<div class="rfq_title">Applicant & Policy Information: </div>
						<div class="driver_hLine">
							<div style="position: absolute; margin-left: 530px; margin-top: -25px;">
								<asp:LinkButton ID="lnkPanel1" runat="server" Text="edit" OnClick="lnkPanel1_Click" />
							</div>
						</div>
						Name:
						<asp:Label ID="LabelName" runat="server" Text="" /><br />
						Email address:
						<asp:Label ID="LabelEmail" runat="server" Text=""></asp:Label><br />
						RV Ownership:
						<asp:Label ID="LabelOwnership" runat="server" Text="" /><br />
						Entity:
						<asp:Label ID="LabelCompanyName" runat="server" Text="" /><br />
						<br />
						Address:<br />
						<asp:Label ID="LabelAddress" runat="server" Text="" />,
						<asp:Label ID="LabelCity" runat="server" Text="" />,
						<asp:Label ID="LabelState" runat="server" Text="" />
						<asp:Label ID="LabelZip" runat="server" Text="" /><br />
						<br />
						Liability Limits Requested:
						<asp:Label ID="LabelLiabilityLimits" runat="server" Text="" /><br />
						Are you a member of an RV Association?:
						<asp:Label ID="LabelRVAssociation" runat="server" Text="" /><br />
						Are you a member of an RV Manufacturer&#8217;s Club?
						<asp:Label ID="LabelManufacturersClub" runat="server" Text="" /><br />
						Is any RV owned by two or more individuals that are not husband and wife?
						<asp:Label ID="LabelOwnedBy" runat="server" Text="" /><br />
						Is any RV used for commercial purposes?
						<asp:Label ID="LabelCommercialUse" runat="server" Text="" /><br />
						Do you use the RV as a primary residence more than 5 months out of the year?
						<asp:Label ID="LabelStationary" runat="server" Text="" /><br />
						What is your current policy Expiration Date?
						<asp:Label ID="LabelExpirationDate" runat="server" Text="" /><br />
						<br />
						<div class="rfq_title">Driver #1 Information: </div>
						<div class="driver_hLine">
							<div style="position: absolute; margin-left: 530px; margin-top: -25px;">
								<asp:LinkButton ID="lnkPanel2a" runat="server" Text="edit" OnClick="lnkPanel2_Click" />
							</div>
						</div>
						Birth Date:
						<asp:Label ID="LabelBirthDate1" runat="server" Text="" /><br />
						Marital Status:
						<asp:Label ID="LabelMaritalStatus1" runat="server" Text="" /><br />
						Does the driver have a valid U.S. Driver&#8217;s License?:
						<asp:Label ID="LabelValidLicense1" runat="server" Text="" /><br />
						Number of Minor Violations in the past 36 months:
						<asp:Label ID="LabelMinViolations1" runat="server" Text="" /><br />
						Number of At Fault Accidents in the past 36 months:
						<asp:Label ID="LabelATFViolations1" runat="server" Text="" /><br />
						Number of Major Violations in the past 36 months:
						<asp:Label ID="LabelMajViolations1" runat="server" Text="" /><br />
						Has the driver completed an RV Safety Driving Course in the past 36 months?:
						<asp:Label ID="LabelRVSafetyCourse1" runat="server" Text="" /><br />
						Does the driver possess a valid Commercial Drivers License (CDL)?:
						<asp:Label ID="LabelCDL1" runat="server" Text="" /><br />
						<br />
						<asp:Panel ID="pnlDriver2" runat="server">
							<div class="rfq_title">Driver #2 Information: </div>
							<div class="driver_hLine">
								<div style="position: absolute; margin-left: 530px; margin-top: -25px;">
									<asp:LinkButton ID="lnkPanel2b" runat="server" Text="edit" OnClick="lnkPanel2_Click" />
								</div>
							</div>
							Birth Date:
							<asp:Label ID="LabelBirthDate2" runat="server" Text="" /><br />
							Marital Status:
							<asp:Label ID="LabelMaritalStatus2" runat="server" Text="" /><br />
							Does the driver have a valid U.S. Driver&#8217;s License?:
							<asp:Label ID="LabelValidLicense2" runat="server" Text="" /><br />
							Number of Minor Violations in the past 36 months:
							<asp:Label ID="LabelMinViolations2" runat="server" Text="" /><br />
							Number of At Fault Accidents in the past 36 months:
							<asp:Label ID="LabelATFViolations2" runat="server" Text="" /><br />
							Number of Major Violations in the past 36 months:
							<asp:Label ID="LabelMajViolations2" runat="server" Text="" /><br />
							Has the driver completed an RV Safety Driving Course in the past 36 months?:
							<asp:Label ID="LabelRVSafetyCourse2" runat="server" Text="" /><br />
							Does the driver possess a valid Commercial Drivers License (CDL)?:
							<asp:Label ID="LabelCDL2" runat="server" Text="" /><br />
							<br />
						</asp:Panel>
						<div class="rfq_title">Vehicle #1 Information: </div>
						<div class="driver_hLine">
							<div style="position: absolute; margin-left: 530px; margin-top: -25px;">
								<asp:LinkButton ID="lnlPanel3a" runat="server" Text="edit" OnClick="lnkPanel3_Click" />
							</div>
						</div>
						Vehicle Type:
						<asp:Label ID="LabelVehicleType1" runat="server" Text="" /><br />
						Vehicle Year:
						<asp:Label ID="LabelVehicleYear1" runat="server" Text="" /><br />
						Vehicle Make:
						<asp:Label ID="LabelVehicleMake1" runat="server" Text="" /><br />
						Vehicle Model:
						<asp:Label ID="LabelVehicleModel1" runat="server" Text="" /><br />
						Original Purchase Price: $<asp:Label ID="LabelVehiclePurchasePrice1" runat="server" Text="" /><br />
						Current Value of the RV: $<asp:Label ID="LabelVehicleCurrentValue1" runat="server" Text="" /><br />
						Are you the original owner of the vehicle:
						<asp:Label ID="LabelVehicleOriginalOwner1" runat="server" Text="" /><br />
						Approximate purchase date of the vehicle:
						<asp:Label ID="LabelVehiclePurchaseDate1" runat="server" Text="" /><br />
						Zip Code where unit was registered:
						<asp:Label ID="LabelVehicleZipReg1" runat="server" Text="" /><br />
						Zip Code where unit is garaged:
						<asp:Label ID="LabelVehicleZipGaraged1" runat="server" Text="" /><br />
						Does the unit have an Anti-Theft device?:
						<asp:Label ID="LabelVehicleAntiTheft1" runat="server" Text="" /><br />
						Does the unit have Air bags?:
						<asp:Label ID="LabelVehicleAirBags1" runat="server" Text="" /><br />
						Does the unit have an Anti-Lock Brakes? :
						<asp:Label ID="LabelVehicleAntiLock1" runat="server" Text="" /><br />
						Preferred deductible to be quoted:
						<asp:Label ID="LabelVehicleDeductible1" runat="server" Text="" /><br />
						Vehicle stationary entire year:
						<asp:Label ID="LabelVehicleStationary1" runat="server" Text="" /><br />
						Fiberglass body:
						<asp:Label ID="LabelVehicleFiberglass1" runat="server" Text="" /><br />
						Vehicle has Rubberized Hail Proof roof:
						<asp:Label ID="LabelHailProof1" runat="server" Text="" /><br />
						<br />
						<asp:Panel ID="pnlVehicle2" runat="server">
							<div class="rfq_title">Vehicle #2 Information: </div>
							<div class="driver_hLine">
								<div style="position: absolute; margin-left: 530px; margin-top: -25px;">
									<asp:LinkButton ID="lnkPanel3b" runat="server" Text="edit" OnClick="lnkPanel3_Click" />
								</div>
							</div>
							Vehicle Type:
							<asp:Label ID="LabelVehicleType2" runat="server" Text="" /><br />
							Vehicle Year:
							<asp:Label ID="LabelVehicleYear2" runat="server" Text="" /><br />
							Vehicle Make:
							<asp:Label ID="LabelVehicleMake2" runat="server" Text="" /><br />
							Vehicle Model:
							<asp:Label ID="LabelVehicleModel2" runat="server" Text="" /><br />
							Original Purchase Price: $<asp:Label ID="LabelVehiclePurchasePrice2" runat="server" Text="" /><br />
							Current Value of the RV: $<asp:Label ID="LabelVehicleCurrentValue2" runat="server" Text="" /><br />
							Are you the original owner of the vehicle:
							<asp:Label ID="LabelVehicleOriginalOwner2" runat="server" Text="" /><br />
							Approximate purchase date of the vehicle:
							<asp:Label ID="LabelVehiclePurchaseDate2" runat="server" Text="" /><br />
							Zip Code where unit was registered:
							<asp:Label ID="LabelVehicleZipReg2" runat="server" Text="" /><br />
							Zip Code where unit is garaged:
							<asp:Label ID="LabelVehicleZipGaraged2" runat="server" Text="" /><br />
							Does the unit have an Anti-Theft device?:
							<asp:Label ID="LabelVehicleAntiTheft2" runat="server" Text="" /><br />
							Does the unit have Air bags?:
							<asp:Label ID="LabelVehicleAirBags2" runat="server" Text="" /><br />
							Does the unit have an Anti-Lock Brakes? :
							<asp:Label ID="LabelVehicleAntiLock2" runat="server" Text="" /><br />
							Preferred deductible to be quoted:
							<asp:Label ID="LabelVehicleDeductible2" runat="server" Text="" /><br />
							Vehicle stationary entire year:
							<asp:Label ID="LabelVehicleStationary2" runat="server" Text="" /><br />
							Fiberglass body:
							<asp:Label ID="LabelVehicleFiberglass2" runat="server" Text="" /><br />
							Vehicle has Rubberized Hail Proof roof:
							<asp:Label ID="LabelHailProof2" runat="server" Text="" /><br />
							<br />
						</asp:Panel>
						<br />
						<%--I'd prefer it if you contacted me by:<br />
                        <asp:DropDownList ID="ddlContactMe" runat="server">
                            <asp:ListItem Text="E-mail" />
                            <asp:ListItem Text="Phone" />
                            <asp:ListItem Text="Fax" />
                        </asp:DropDownList>
                        <br />
                        <br />--%>
					</asp:Panel>
					<asp:Panel ID="pnlControls" runat="server" Visible="true">
						<div style="width: 640px; text-align: right;">

							<asp:Button ID="Button1" runat="server" Text="Print" CssClass="common_button" OnClientClick="javascript:alertStatus=false;window.print();" />
							<asp:Button ID="cmdBack" runat="server" Text="Back" CssClass="common_button" OnClick="cmdBack_Click" OnClientClick="javascript:alertStatus=false" />
							<asp:Button ID="cmdSubmit" runat="server" Text="Submit" CssClass="common_button" OnClick="cmdSubmit_Click" OnClientClick="javascript:alertStatus=false" />
						</div>
					</asp:Panel>
				</div>
			</div>
		</div>
	</div>

</asp:Content>
