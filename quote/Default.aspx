﻿<%@ Page Language="C#" MasterPageFile="~/AppQuote.master" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
	<title>RV Insurance | Motorhome Insurance | Blue Sky RV Insurance</title>
	<meta http-equiv="Keywords" name="Keywords" content="RV Insurance, Motorhome Insurance, Blue Sky RV Insurance" />
	<meta http-equiv="Description" name="Description" content="Blue Sky RV has put years of RV insurance experience to work to offer specialty RV and motorhome insurance coverage policies for all types of RVs and motorhomes." />
	<script type="text/javascript">
		$(document).ready(function () { $('#Tab1').addClass("selected"); });
	</script>

    <!-- Start Quantcast Tag -->
    <script type="text/javascript">
        var _qevents = _qevents || [];

        (function () {
            var elem = document.createElement('script');
            elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
            elem.async = true;
            elem.type = "text/javascript";
            var scpt = document.getElementsByTagName('script')[0];
            scpt.parentNode.insertBefore(elem, scpt);
        })();

        _qevents.push(
        { qacct: "p-VypNfW3as3_Ed", labels: "_fp.event.Homepage" }
        );
    </script>
    <noscript>
    <img src="//pixel.quantserve.com/pixel/p-VypNfW3as3_Ed.gif?labels=_fp.event.Homepage" style="display: none;" border="0" height="1" width="1" alt="Quantcast"/>
    </noscript>
    <!-- End Quantcast tag -->
</asp:Content>
<asp:Content ContentPlaceHolderID="hero" runat="server">
    <section class="full_background clouds_mountain scenic_background">
	    <div class="bl_trans_bg">
            <div class="container">
                <div class="row">
                    <div class="column">
                        <h4>Blue Sky RV Insurance Quote</h4>
                        <p>The Blue Sky RV Insurance program is currently not available for vehicles registered in the following states: AL, AK, CT, DE, FL, HI, KY, MA, MD, ME, MS, NH, NY, RI, VT and WY. If your registration state is listed above please check back soon as we are continuing to expand and add new states to serve you. Coverage is also not available on stationary trailers or park model travel trailers that are registered or garaged in GA, LA, NC, NJ, SC, TX, and VA. </p>
                    </div>
                </div>
            </div>
	    </div>
    </section>
</asp:Content>
<asp:Content ContentPlaceHolderID="main" runat="server">
    <a href="rv-insurance-quote-intro.aspx" class="button">start quote</a>
	<%--<div class="content" style="height:560px">
		<div class="company_info" style="height:514px">
			<img src="images/default_bg4.jpg" alt="RV Insurance, Blue Sky RV Insurance" />
		</div>
		<div class="text">
			<div style="float:left; width:510px; height:310px;"></div>
			<h1>Blue Sky RV Insurance</h1>
			<h2>Affordable Coverage for your Motorhome<br />or travel trailer</h2>
			<p >Blue Sky has saved money for thousands of motorhome owners just like you.  Request your free quote today to see how much you could save on the insurance for your motorhome or travel trailer.</p>
			<h2>Innovative RV Insurance Products</h2>
			<p>The Blue Sky RV Insurance Program offers new and exciting specialty RV insurance coverages unavailable elsewhere.  Blue Sky is the first to offer a true depreciation free policy and the innovator of consignment coverage for your RV.</p>
			<h2>Financial Strength</h2>
			<p>Underwritten by RLI, a specialty insurance company rated A+ for financial strength by A.M. Best and Standard & Poor&#8217;s, the Blue Sky RV Insurance Program provides products of the highest financial standards.</p>
			<h2>Recreation Insurance Specialists Opens West Coast Office</h2>
				<div style="float:right; margin:0px 10px 0px 20px">
					<a id="bbblink" class="ruvtbum" href="http://www.bbb.org/akron/business-reviews/insurance-agency/blue-sky-rv-insurance-in-akron-oh-90020867#bbblogo" title="Blue Sky RV Insurance, Insurance Agency, Akron, OH" style="display: block;position: relative;overflow: hidden; width: 60px; height: 108px; margin: 0px; padding: 0px;"><img style="padding: 0px; border: none;" id="bbblinkimg" src="http://seal-akron.bbb.org/logo/ruvtbum/blue-sky-rv-insurance-90020867.png" width="120" height="108" alt="Blue Sky RV Insurance, Insurance Agency, Akron, OH" /></a><script type="text/javascript">var bbbprotocol = (("https:" == document.location.protocol) ? "https://" : "http://"); document.write(unescape("%3Cscript src='" + bbbprotocol + 'seal-akron.bbb.org' + unescape('%2Flogo%2Fblue-sky-rv-insurance-90020867.js') + "' type='text/javascript'%3E%3C/script%3E"));</script>
				</div>
			<p>
				Effective October 1, 2012, Recreation Insurance Specialists LLC (&#8220;RIS&#8221;), the program administrator for the Blue Sky RV Insurance Program, has opened an office in Tucson, Arizona. This office will be staffed by Rich Wiersma and Jim Schecter, two long time RV specialty agents previously employed by Mobility Insurance Coverage, Inc. (&#8220;Mobility&#8221;), an Arizona licensed RV specialty insurance agency. In conjunction with opening this office in Tucson, RIS purchased select assets previously belonging to Mobility, including the renewal rights to the in force business previously produced by Mobility. This new office will also allow RIS to eventually increase the hours of service available to our Blue Sky agents and customers.
			</p>--%>

			<%--<div style="float:left; width:510px; height:300px;"></div>
			<h1>Blue Sky RV Insurance</h1>
			<h2>Going the extra mile with policies for motorhomes, campers and more</h2>
			<p>America's open roads call for the peace of mind you'll get with Blue Sky RV Insurance from Recreation Insurance Specialists. Our singular focus on recreation vehicles enables us to bring you not only standard policies, but also specialty coverages that are hard to find elsewhere.</p>
			<p>As insurance underwriters with 40 years of experience in the recreational vehicle industry, we pride ourselves on meeting tough challenges. You can rely on us for innovative RV policies including depreciation-free, total loss deductible buy-back, and diminishing deductible express. Our specialty RV insurance also extends to policies with total loss replacement, diminishing deductibles, personal effects, and more.</p>
			<p>Whether your ideal get-away vehicle is a Class A motor home, mini-motor home (Class C), pop-up camper, bus conversion, camper van or travel trailer, we have the RV insurance policy to protect you. Of course, as recreational vehicle specialists, we also cover medium-duty tow vehicles, toterhomes, fifth wheel campers, truck campers and utility trailers, as well as your personal cars and pickup trucks.</p>
			<p>Recreation Insurance Specialists serves all states with Blue Sky RV Insurance, including:
			<ul>
			<li>Standard RV insurance for liability protection such as bodily injury, property damage, uninsured motorists, medical payments, and personal injury protection</li>
			<li>Innovative RV insurance such as depreciation-free, total loss deductible buy-back, and diminishing deductible express</li>
			<li>Specialty RV insurance including total loss replacement, diminishing deductibles, personal effects, and more</li>
			</ul>
		</div>

		<div class="quoteBanner" style="top:535px"><a href="RV-Insurance-Quote-intro.aspx"></a></div>

		<div class="home_banner_container">
			<div class="home_banner" id="news" onclick="window.location='RV-Insurance-Coverage.aspx?isOn=200';" onmouseover="menuButtonsOn('news');" onmouseout="menuButtonsOff('news');">
				<div class="home_banner_news" id="news_on"></div>
				<div class="home_banner_text">Coverage that<br />
					points you in the right direction</div>
			</div>
			<div class="home_banner" id="badge" onclick="window.location='Recreational-Vehicles.aspx?isOn=300';" onmouseover="menuButtonsOn('badge');" onmouseout="menuButtonsOff('badge');">
				<div class="home_banner_badge" id="badge_on"></div>
				<div class="home_banner_text">Recreational<br />
					Vehicle types<br />
					that we insure</div>
			</div>
			<div class="home_banner" id="checkBox" onclick="window.location='RV-Insurance-Claims.aspx?isOn=6';" onmouseover="menuButtonsOn('checkbox');" onmouseout="menuButtonsOff('checkbox');">
				<div class="home_banner_checkBox" id="checkbox_on"></div>
				<div class="home_banner_text">What you need<br />
					to know about reporting claims</div>
			</div>
		</div>

		

	</div>--%>
</asp:Content>
